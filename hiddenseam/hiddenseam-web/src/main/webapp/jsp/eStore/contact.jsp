<div id="container">
<!-- mobile actions -->
<section id="mobile_bar" class="clearfix">
    <div class="mobile_menu_control icon-menu"></div>
    <div class="top_bar_control icon-cog"></div>
</section>
<!-- / end section #mobile_bar -->

<div id="header_container">
    <!-- top bar -->
    <section id="top_bar" class="clearfix">
        <div class="top_bar_container">
            <div>
                <a href="https://www.facebook.com/hiddenseam"><img
                        src="http://hiddenseam.com/wp-content/themes/rttheme18/images/facebook.png"></a>
                <a href="https://www.instagram.com/hiddenseam/"><img
                        src="http://hiddenseam.com/wp-content/themes/rttheme18/images/instagram.png"></a>
            </div>
            <ul id="top_navigation" class="top_links">
                <li class="icon-login"><a href="http://hiddenseam.com/my-account/" title="Login / Register">Login /
                    Register</a></li>
                <li class="icon-basket">
                    <span>
	                    <a class="cart-contents" href="http://hiddenseam.com/cart-3/" title="View Cart">
                            0 items- <span class="amount">0.00Rs</span>
                        </a>
	                </span>&nbsp;
                </li>
            </ul>
        </div>
    </section>

    <!-- header -->
    <header id="header">
        <!-- header contents -->
        <section id="header_contents" class="clearfix">
            <section class="section_widget first three"></section>
            <!-- end section .section_widget -->
            <section class="section_logo logo_center three">
                <!-- logo -->
                <section id="logo">
                    <a href="http://hiddenseam.com" title="Hidden Seam"><img
                            src="http://hiddenseam.com/wp-content/uploads/2015/09/logo-low-res1.png"
                            alt="Hidden Seam" data-retina=""></a>
                </section>
                <!-- end section #logo -->
            </section>
            <!-- end section #logo -->
            <section class="section_widget second three">
                <section id="slogan_text" class="right_side ">
                </section>
            </section>
            <!-- end section .section_widget -->
        </section>
        <!-- end section #header_contents -->

        <!-- navigation -->
        <div class="nav_shadow  original" style="visibility: visible;">
            <div class="nav_border">
                <nav id="navigation_bar" class="navigation ">
                    <ul id="navigation" class="menu">
                        <li id="menu-item-2920"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children hasSubMenu top-level-0"
                            data-column-size="0"><a href="http://hiddenseam.com/category/">Shop</a>
                            <ul class="sub-menu">
                                <li id="menu-item-2967"
                                    class="menu-item menu-item-type-custom menu-item-object-custom"><a
                                        href="http://hiddenseam.com/product-category/shirts/">Shirts</a></li>
                                <li id="menu-item-2968"
                                    class="menu-item menu-item-type-custom menu-item-object-custom"><a
                                        href="http://hiddenseam.com/product-category/tops/">Tops</a></li>
                                <li id="menu-item-2969"
                                    class="menu-item menu-item-type-custom menu-item-object-custom"><a
                                        href="http://hiddenseam.com/product-category/tshirts/">T-Shirts</a></li>
                                <li id="menu-item-2970"
                                    class="menu-item menu-item-type-custom menu-item-object-custom"><a
                                        href="http://hiddenseam.com/product-category/dresses/">Dresses</a></li>
                                <li id="menu-item-4116"
                                    class="menu-item menu-item-type-custom menu-item-object-custom"><a
                                        href="http://hiddenseam.com/product-category/accessories/">Accessories</a>
                                </li>
                            </ul>
                        </li>
                        <li id="menu-item-3545"
                            class="menu-item menu-item-type-post_type menu-item-object-page top-level-1"
                            data-column-size="0"><a href="http://hiddenseam.com/look-book-4/">LookBook</a></li>
                        <li id="menu-item-2822"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children hasSubMenu top-level-2"
                            data-column-size="0"><a href="http://hiddenseam.com/about/">About</a>
                            <ul class="sub-menu">
                                <li id="menu-item-2990"
                                    class="menu-item menu-item-type-post_type menu-item-object-page"><a
                                        href="http://hiddenseam.com/exchange-policy/">Exchange Policy</a></li>
                                <li id="menu-item-2989"
                                    class="menu-item menu-item-type-post_type menu-item-object-page"><a
                                        href="http://hiddenseam.com/privacy-policy/">Privacy Policy</a></li>
                                <li id="menu-item-2988"
                                    class="menu-item menu-item-type-post_type menu-item-object-page"><a
                                        href="http://hiddenseam.com/terms-of-service/">Terms of Service</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-3608"
                            class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2993 current_page_item top-level-3"
                            data-column-size="0"><a href="http://hiddenseam.com/contact/">Contact</a></li>
                        <li id="menu-item-4246"
                            class="menu-item menu-item-type-post_type menu-item-object-page top-level-4"
                            data-column-size="0"><a href="http://hiddenseam.com/?page_id=2462">Eshop</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- / navigation  -->
    </header>
    <!-- end tag #header -->
</div>


<!-- content holder -->
<div class="content_holder">
    <div class="content_second_background">
        <div class="content_area clearfix">
            <div id="row-334065-1" class="content_block_background template_builder "
                 style="background-attachment: scroll;">
                <section class="content_block clearfix">
                    <section id="row-334065-1-content" class="content full  clearfix">
                        <section class="info_bar clearfix only_breadcrumb">
                            <section class="breadcrumb">
                                <div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a
                                        class="icon-home" href="http://hiddenseam.com" itemprop="url"><span
                                        itemprop="title" title="Hidden Seam">Home</span></a> <span
                                        class="icon-angle-right"></span>

                                    <div itemscope="" itemprop="child"
                                         itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"
                                                                                                title="Contact">Contact</span>
                                    </div>
                                </div>
                            </section>
                        </section>
                        <div class="row clearfix">
                            <div class="box three ">

                                <h3 class="featured_article_title">GET IN TOUCH</h3>

                                <div class="space margin-b20"></div>
                                <ul class="with_icons default " data-rt-animation-group="group">
                                    <li class="box one first last" data-rt-animate="animate"
                                        data-rt-animation-type="fadeInDown"><span
                                            class="icon-phone-squared icon"></span>

                                        <p>080 22231224</p></li>
                                    <li class="box one first last" data-rt-animate="animate"
                                        data-rt-animation-type="fadeInDown"><span class="icon-mail icon"></span>

                                        <p>getsocial@hiddenseam.com</p></li>
                                </ul>
                            </div>
                            <div class="box two-three ">
                                <div role="form" class="wpcf7" id="wpcf7-f4-o1" lang="en-US" dir="ltr">
                                    <div class="screen-reader-response"></div>
                                    <form name="" action="/contact/#wpcf7-f4-o1" method="post" class="wpcf7-form"
                                          novalidate="novalidate">
                                        <div style="display: none;">
                                            <input type="hidden" name="_wpcf7" value="4">
                                            <input type="hidden" name="_wpcf7_version" value="4.1.2">
                                            <input type="hidden" name="_wpcf7_locale" value="en_US">
                                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4-o1">
                                            <input type="hidden" name="_wpnonce" value="06da834551">
                                        </div>
                                        <p>Your Name (required)<br>
                                                <span class="wpcf7-form-control-wrap your-name"><input type="text"
                                                                                                       name="your-name"
                                                                                                       value=""
                                                                                                       size="40"
                                                                                                       class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                       aria-required="true"
                                                                                                       aria-invalid="false"></span>
                                        </p>

                                        <p>Your Email (required)<br>
                                                <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                                                        name="your-email"
                                                                                                        value=""
                                                                                                        size="40"
                                                                                                        class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                        aria-required="true"
                                                                                                        aria-invalid="false"></span>
                                        </p>

                                        <p>Subject<br>
                                                <span class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                                          name="your-subject"
                                                                                                          value=""
                                                                                                          size="40"
                                                                                                          class="wpcf7-form-control wpcf7-text"
                                                                                                          aria-invalid="false"></span>
                                        </p>

                                        <p>Your Message<br>
                                                <span class="wpcf7-form-control-wrap your-message"><textarea
                                                        name="your-message" cols="40" rows="10"
                                                        class="wpcf7-form-control wpcf7-textarea"
                                                        aria-invalid="false"></textarea></span></p>

                                        <p><input type="submit" value="Send"
                                                  class="wpcf7-form-control wpcf7-submit"><img class="ajax-loader"
                                                                                               src="http://hiddenseam.com/wp-content/plugins/contact-form-7/images/ajax-loader.gif"
                                                                                               alt="Sending ..."
                                                                                               style="visibility: hidden;">
                                        </p>

                                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </section>
            </div>


        </div>
        <!-- / end div .content_area -->


        <div class="content_footer footer_widgets_holder footer-334065">
            <section class="footer_widgets clearfix footer-widgets-334065 clearfix"></section>
        </div>


    </div>
    <!-- / end div .content_second_background -->


</div>
<!-- / end div .content_holder -->


</div>