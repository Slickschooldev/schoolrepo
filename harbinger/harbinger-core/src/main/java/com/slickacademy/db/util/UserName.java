package com.slickacademy.db.util;

public class UserName {
    public static final ThreadLocal<String> userName = new ThreadLocal<String>();
    
    public static  void setUserName(String user){
        if(userName!=null){
            userName.remove();
        }
        userName.set(user);
     }
    
    public static String getUserName(){
        return userName.get();
    }
}
