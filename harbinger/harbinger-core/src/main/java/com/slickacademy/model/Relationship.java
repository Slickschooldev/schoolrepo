package com.slickacademy.model;

/**
 * Created by govind on 30/6/15.
 */
public enum Relationship {
    FATHER(0),
    MOTHER(1),
    GAURDIAN(2);

    private final int i;

    private Relationship(int i) {
        this.i = i;
    }
}
