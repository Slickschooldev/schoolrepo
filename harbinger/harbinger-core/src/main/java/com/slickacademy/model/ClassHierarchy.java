package com.slickacademy.model;

/**
 * Created by govind on 30/6/15.
 */
public class ClassHierarchy {

    private long id;
    private SchoolHierarchy schoolHierarchy;
    private String section;
    private Faculty faculty;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SchoolHierarchy getSchoolHierarchy() {
        return schoolHierarchy;
    }

    public void setSchoolHierarchy(SchoolHierarchy schoolHierarchy) {
        this.schoolHierarchy = schoolHierarchy;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

}
