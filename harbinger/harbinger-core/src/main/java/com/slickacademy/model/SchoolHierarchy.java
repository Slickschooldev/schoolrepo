package com.slickacademy.model;

/**
 * Created by govind on 30/6/15.
 */
public class SchoolHierarchy {
    private long id;
    private School school;
    private Class clazz;

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }
}
