package com.slickacademy.properties.util;

import com.slickacademy.context.ApplicationContextProvider;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

/**
 * Created by ganesh on 8/6/15.
 */
public class ConfigPropertiesListener implements ServletContextListener {
    private Properties properties = new Properties();
    private FileInputStream fileInputStream;


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        String appPath = servletContext.getRealPath("");
        System.out.println("context path " + servletContext.getContextPath());
        String appconfigPath = appPath.substring(0,
                appPath.indexOf("webapps"));
        try {
            System.out.println(appPath);
            File propertiesFile = new File(appconfigPath + "/config.properties");
            if (propertiesFile.exists()) {
                properties.load(new FileInputStream(appconfigPath + "/config.properties"));

                File newPropertyFile = new File(appPath + File.separator
                        + "WEB-INF" + File.separator + "classes" + File.separator
                        + "default.properties");

                if (!newPropertyFile.exists()) {
                    throw new Exception(
                            "Default Configuration file is empty...");
                }
                FileOutputStream fileOutputStream = new FileOutputStream(newPropertyFile);

                Properties allProps = new Properties();

                fileInputStream = new FileInputStream(propertiesFile);
                allProps.load(fileInputStream);

                Set<Object> allPropertyKeys = allProps.keySet();


                PropertiesConfiguration propertiesConfiguration = new PropertiesConfiguration();
                for (Object key : allPropertyKeys) {
                    String tmp = null;
                    String enc = "ENC(";
                    String value = allProps.getProperty(key
                            .toString());
                    if (value.startsWith(enc) && value.endsWith(")")) {
                        value = value.substring(enc.length(),
                                value.length() - 1);

                        StandardPBEStringEncryptor decryptor = (StandardPBEStringEncryptor) ApplicationContextProvider
                                .getApplicationContext().getBean("stringEncryptor");
                        tmp = decryptor.decrypt(value);
                        allProps.setProperty(key.toString(), tmp);
                    }
                    propertiesConfiguration.setProperty(key.toString(), allProps.getProperty(key.toString()));
                }

                propertiesConfiguration.save(fileOutputStream);
                System.out.println(properties.getProperty("test"));
                ConfigProperties.getInstance().setProperties(allProps);
            } else {
                throw new Exception(
                        "Configuration file is empty...");
            }
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
