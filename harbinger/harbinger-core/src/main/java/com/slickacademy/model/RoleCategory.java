package com.slickacademy.model;

/**
 * Created by govind on 30/6/15.
 */
public enum RoleCategory {
    STUDENT(0),
    FACULTY(1);

    private final int i;

    private RoleCategory(int i) {
        this.i = i;
    }

}
