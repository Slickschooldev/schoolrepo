<%@ page import="com.slickacademy.db.util.PersistenceManager" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.HashSet" %>
<%@ page import="com.slickacademy.model.Currency" %>
<%@ page import="com.slickacademy.ecomm.model.Product" %>
<%@ page import="com.slickacademy.ecomm.model.ProductCategory" %>
<%
    PersistenceManager pm = PersistenceManager.getInstance();
    Product product = new Product();
    Set<String> colors = new HashSet<String>();
    colors.add("yellow");
    colors.add("white");
    Set<String> sizes = new HashSet<String>();
    sizes.add("21 IN");
    sizes.add("23 US");
    Set<String> productImageUrls = new HashSet<String>();
    ProductCategory productCategory = new ProductCategory();
    productCategory.setId(1);
    productImageUrls.add("testUrl1");
    productImageUrls.add("testUrl2");
    product.setColors(colors);
    product.setCurrency(Currency.RUPEE);
    product.setDescription("test");
    product.setPrice(213.22);
    product.setProductCategory(productCategory);
    product.setProductImageUrls(productImageUrls);
    product.setSizes(sizes);
    pm.insertDetails(product);
%>