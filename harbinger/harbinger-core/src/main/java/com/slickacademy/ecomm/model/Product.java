package com.slickacademy.ecomm.model;

import com.slickacademy.db.util.BaseEntity;
import com.slickacademy.model.Currency;

import java.util.Set;

/**
 * Created by hemanth on 15/5/16.
 */
public class Product extends BaseEntity{
    private ProductCategory productCategory;
    private Set<String> sizes;
    private Set<String> colors;
    private double price;
    private Currency currency;
    private String description;
    private String productDescription;
    private Set<String> productImageUrls;

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Set<String> getSizes() {
        return sizes;
    }

    public void setSizes(Set<String> sizes) {
        this.sizes = sizes;
    }

    public Set<String> getColors() {
        return colors;
    }

    public void setColors(Set<String> colors) {
        this.colors = colors;
    }

    public Set<String> getProductImageUrls() {
        return productImageUrls;
    }

    public void setProductImageUrls(Set<String> productImageUrls) {
        this.productImageUrls = productImageUrls;
    }
}
