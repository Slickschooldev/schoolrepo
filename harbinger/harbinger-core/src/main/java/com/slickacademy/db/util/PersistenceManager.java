package com.slickacademy.db.util;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import com.slickacademy.context.ApplicationContextProvider;
import com.slickacademy.exception.CommonError;
import com.slickacademy.exception.HarbingerException;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleObjectStateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.DataException;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.exception.SQLGrammarException;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.ReflectionUtils;

import com.mchange.v2.c3p0.PooledDataSource;

//import com.zinnia.core.auditlog.AuditLogInterceptor;

/**
 * Hibernate utility class for CRUD operations on entities
 * 
 * @author dev
 * 
 */

@Repository
public class PersistenceManager extends Manager {
    private String  entityName         = null;
    private static PersistenceManager instance = null, instance2 = null;
    private int instanceIndex;
    private PooledDataSource pds = null;

    private ApplicationContext applicationContext = null;
    private int maxConn = 1;

    @Autowired
    private SessionFactory sessionFactory = null;
    /**
     * default loged in user
     */
    public static final String DEFAULT_USERNAME = "System Initiated";
    
    /**
     * string constants
     */
    private static final String FROM             = "from";
    /**
     * string constants
     */
    private static final String AS_ENTITY        = "as entity";
    /**
     * string constants
     */
    private static final String WHERE            = "where";
    /**
     * context for this instance of persistence manager
     */
    private String              context;
    /**
     * logger
     */
    private static Logger logger = Logger.getLogger(PersistenceManager.class);

    /**
     * 
     * @return context
     */
    public String getContext() {
        return context;
    }

    /**
     * 
     * @param context
     *            by default is hibernate-cfg.xml
     */
    public void setContext(String context) {
        this.context = context;
    }

    /**
     * intializes the with hibernate configuration file
     */
    public synchronized void init() {
    	pds = (PooledDataSource) SessionFactoryUtils.getDataSource(sessionFactory);
    }

    /**
     * 
     */
    private PersistenceManager() {
        super();
    }

    /**
     * 
     * @return instance of persistenceManager
     */
    public static synchronized PersistenceManager getInstance() {
    	if(instance == null) { 
    		instance = new PersistenceManager();
    		instance.instanceIndex = 1;
    		if(logger.isDebugEnabled()) {
    			logger.debug("Persistence manager instantiated");
    		}
    	}
        return instance;
    }
    
    public static synchronized PersistenceManager getSecondInstance() {
    	if(instance2 == null) { 
    		instance2 = new PersistenceManager();
    		instance2.instanceIndex = 2;
    		if(logger.isDebugEnabled()) {
    			logger.debug("Persistence manager for second connection pool instantiated");
    		}
    	}
        return instance2;
    }


    public Connection getJDBCConnection() {
        Connection connection = null;
        try {
            String driver = "com.mysql.jdbc.Driver";
            String url    = "jdbc:mysql://localhost:3306/testSlick";
            String username = "root";
            String password = "avanseus$0";
            System.setProperty(driver,"");
            connection = DriverManager.getConnection(url,username,password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

	public Session openSession() throws HarbingerException {

		if(logger.isTraceEnabled()) { 
			logger.trace("Using instance-" + instanceIndex + " of Persistence Manager" );
		}
		
    	try {
    		if(pds != null && pds.getNumBusyConnectionsDefaultUser() == maxConn) { 
    			logger.warn("Trying to acquire a connection, but all the available connections, i.e. " + maxConn  + " connections, are currently busy" );
    		}
		} catch (SQLException e) {
			logger.warn("Caught an exception while trying to check the number of busy connections");
		}
    	
    	Session session = null;
    	try {
    		if (TransactionSynchronizationManager.hasResource(sessionFactory)) {
    			SessionHolder sessionHolder = (SessionHolder) TransactionSynchronizationManager.getResource(sessionFactory);
    			session = sessionHolder.getSession();
    			if(logger.isTraceEnabled()) { 
    				logger.trace("Using existing Session in PersistenceManager");
    			}
    		}
    		else {
    			if(logger.isTraceEnabled()) { 
    				logger.trace("Opening Session in PersistenceManager : Thread ID : " + Thread.currentThread().getId());
    			}
    			session = getSession(sessionFactory);
    			if(logger.isTraceEnabled()) { 
    				logger.trace("Done with Open Session in PersistenceManager : Thread ID : " + Thread.currentThread().getId());
    			}
    		}
    	} catch(HibernateException ex) { 
    		throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    "Failed to get the current session", ex);
    	}
    	
    	return session;
    }
    
	/**
	 * @param sessionFactory
	 * @return
	 * @throws HarbingerException
	 */
	protected Session getSession(SessionFactory sessionFactory) throws HarbingerException {
		return SessionFactoryUtils.getSession(sessionFactory, null, null);
	}
    
    public void closeSession(Session session) throws HarbingerException {
    	if(session != null) { 
    		try {
    			if (TransactionSynchronizationManager.hasResource(sessionFactory)) {
    				if(logger.isTraceEnabled()) { 
    					logger.trace("Not closing Session used in transaction, in PersistenceManager");
    				}
    			}
    			else {
    				if(logger.isTraceEnabled()) { 
    					logger.trace("Closing Session in PersistenceManager : Thread ID : " + Thread.currentThread().getId());
    				}
    				session.close();
    				if(logger.isTraceEnabled()) { 
    					logger.trace("Done with close Session in PersistenceManager : Thread ID : " + Thread.currentThread().getId());
    				}
    			}
    		} catch(HibernateException ex) { 
    			throw new HarbingerException(CommonError.DB_SESSION_ERROR,
    					"Failed to close the session", ex);
    		}
    	}
    }

    /**
     * @param entityName
     *            fully qualified class name of the entity
     * @return fetches all the values for the specified entityName
     * @throws HarbingerException
     *             thrown when the getAllDetails query execution fails
     */
    public List<?> getAllDetails(String entityName)
            throws HarbingerException {
        Session session = null;
        session = openSession();
        List<?> list = null;
        try {
            StringBuffer sb = new StringBuffer();
            sb.append(FROM + " ");
            sb.append(entityName + " ");
            sb.append(AS_ENTITY + " ");

            Query q = session.createQuery(sb.toString());
            list = q.list();
            session.flush();
        } catch (HibernateException ex) {
            throw new HarbingerException(CommonError.QUERY_ERROR,
                    ex.getMessage(), ex);
        } finally {
        	if(session!=null) { 
        		closeSession(session);
        	}
        }
        return list;
    }

    /**
     * @param id
     *            id of the entity
     * @param queryName
     *            name of the named-query
     * @return fetches entity object with given id . If no object found returns
     *         null
     * @throws HarbingerException
     *             thrown when search query fails
     */
    public Object searchDetails(long id, String queryName)
            throws HarbingerException {
        Session session = null;
        session = openSession();
        Object obj = null;
        try {
            Query q = session.getNamedQuery(queryName);
            q.setLong("id", id);
            List<?> l = q.list();
            if (l != null && l.size() > 0) {
                obj = l.get(0);
            } 
			session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            }
        } finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return obj;
    }

    /**
     * @param id
     *            id of the entity
     * @param queryName
     *            name of the named-query
     * @return fetches entity object ,in a list , with given id . If no object
     *         found returns null
     * @throws HarbingerException
     *             thrown when search all details query fails
     */
    public List<?> searchAllDetails(long id, String queryName)
            throws HarbingerException {
        Session session = openSession();
        List<?> l = null;
        try {
            Query q = session.getNamedQuery(queryName);
            q.setLong("id", id);
            l = q.list();
            session.flush();
        } catch (StaleObjectStateException ex) {
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR, ex.getMessage(), ex);
        } catch (HibernateException ex) {
        if (ex.getCause() instanceof ConstraintViolationException) {
            throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR, ex.getMessage(), ex);
        } else if (ex.getCause() instanceof DataException) {
            throw new HarbingerException(CommonError.DATA_EXCEPTION, ex.getMessage(), ex);
        } else if (ex.getCause() instanceof JDBCConnectionException) {
            throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION, ex.getMessage(), ex);
        } else if (ex.getCause() instanceof SQLGrammarException) {
            throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT, ex.getMessage(), ex);
        } else {
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
        }
    }finally{
            if (session != null) {
                closeSession(session);
            }
        }
        return l;
    }

    /**
     * @param entityName
     *            fully qualified class name of the entity
     * @param baseEntity
     *            entity instance
     * @return fetches the entities which look similar to {@link BaseEntity}
     * @throws HarbingerException
     *             thrown when search query execution has failed
     */
    public List<?> searchDetails(String entityName, BaseEntity baseEntity)
            throws HarbingerException {
        Session session = null;
        session = openSession();
        List<?> list = null;
        try {
            final Criteria criteria = session.createCriteria(Class
                    .forName(entityName));

            if (baseEntity.getDescription() != null
                    && (!baseEntity.getDescription().equals(""))) {
                criteria.add(Restrictions.ilike("description",
                        baseEntity.getDescription(), MatchMode.START));
            }
            if (baseEntity.getName() != null
                    && (!baseEntity.getName().equals(""))) {
                criteria.add(Restrictions.ilike("name", baseEntity.getName(),
                        MatchMode.START));
            }
            if (baseEntity.getId() > 0) {
                criteria.add(Restrictions.eq("id", baseEntity.getId()));
            }
            if (baseEntity.getEnteredBy() != null
                    && (!baseEntity.getEnteredBy().equals(""))) {
                criteria.add(Restrictions.eq("enteredBy",
                        baseEntity.getEnteredBy()));
            }
            /*
             * Modified by Ganesh K T,21,March,2011 to show all the rows
             */
            list = criteria.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            }
        }catch (ClassNotFoundException ex) {
                throw new HarbingerException(
                        CommonError.CLASS_NOT_FOUND_ERROR, ex.getMessage(), ex);
        }finally {
            closeSession(session);
        }
        return list;
    }

    /**
     * @param entityName
     *            fully qualified class name of the entity
     * @param id
     *            entity id
     * @param name
     *            entity name
     * @return fetches entities with given id and name
     * @throws HarbingerException
     *             thrown when search query execution fails
     */
    public List<? extends BaseEntity> searchDetails(String entityName,
            long id, String name) throws HarbingerException {
        Session session = openSession();
        List<? extends BaseEntity> l = null;
        try {
            StringBuffer sb = new StringBuffer(FROM + " ");
            sb.append(entityName);
            sb.append(" " + AS_ENTITY + " where entity.id = ");
            sb.append(id);
            sb.append(" or entity.name like '");
            sb.append(name);
            sb.append("%'");

            Query q = null;
            q = session.createQuery(sb.toString());

            l = q.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        } finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return l;
    }

    /**
     * @param entityName
     *            fully qualified class name of the entity
     * @param id
     *            entity id
     * @return fetches entities with given id . It only works for NamedEntities
     * @throws HarbingerException
     *             thrown in case the search query execution fails
     */

    public BaseEntity searchDetails(String entityName, long id)
            throws HarbingerException {
        Session session = openSession();
        BaseEntity ne = null;
        try {
            StringBuffer sb = new StringBuffer(FROM + " ");
            sb.append(entityName);
            sb.append(" " + AS_ENTITY + " where entity.id = ");
            sb.append(id);
            Query q = session.createQuery(sb.toString());
            List<? extends BaseEntity> l = q.list();
            if (l !=null && l.size() > 0) {
                ne = (BaseEntity) l.get(0);
            } 
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return ne;
    }

    public Object searchEntity(String entityName, long id)
            throws HarbingerException {
        Session session = openSession();
        Object obj = null;
        try {
            StringBuffer sb = new StringBuffer(FROM + " ");
            sb.append(entityName);
            sb.append(" " + AS_ENTITY + " where entity.id = ");
            sb.append(id);
            Query q = session.createQuery(sb.toString());
            List<? extends BaseEntity> l = q.list();
            if (l !=null && l.size() > 0) {
                obj = l.get(0);
            }
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
                throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                        ex.getMessage(), ex);
            }
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return obj;
    }

    /**
     * @param entityName
     *            fully qualified class name of the entity
     * @param id
     *            entity id
     * @return fetches entities with given id . It works for any kind of entity.
     * @throws HarbingerException
     *             thrown when search query fails execution
     */

    public Object searchDetailsForAnonymousEntity(String entityName, long id)
            throws HarbingerException {
        Session session = null;
        Object obj = null;
        try {
        	session = openSession();
            StringBuffer sb = new StringBuffer(FROM + " ");
            sb.append(entityName);
            sb.append(" " + AS_ENTITY + " where entity.id = ");
            sb.append(id);
            Query q = session.createQuery(sb.toString());
            List<?> l = q.list();
            if (l !=null && l.size() > 0) {
                obj = l.get(0);
            } 
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        } finally {
           if (session != null) {
                closeSession(session);
           }
        }
        return obj;
    }

    /**
     * @param entityName
     *            fully qualified class name of the entity
     * @return fetches all the values for the specified BaseEntity
     * @throws HarbingerException
     *             thrown when search query fails in execution
     */

    public List<? extends BaseEntity> searchDetails(String entityName)
            throws HarbingerException {
        Session session = openSession();
        List<? extends BaseEntity> l = null;
        try {
            StringBuffer sb = new StringBuffer(FROM + " ");
            sb.append(entityName);
            sb.append(" " + AS_ENTITY + " ");

            Query q = null;
            q = session.createQuery(sb.toString());

            l = q.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
        	if (session != null) {
                closeSession(session);
            }
        }
        return l;
    }

    /**
     * @param queryName
     *            name of the named query
     * @param params
     *            named parameters for the query
     * @return fetches values for the specified entity using specified named
     *         query
     * @throws HarbingerException
     *             if an exception occurs in executing search query
     */
    public List<?> searchByNamedQuery(String queryName,
            Map<String, Object> params) throws HarbingerException {
        Session session = openSession();
        List<?> results = null;
        try {
            final Query query = session.getNamedQuery(queryName);
            if (params != null && params.size() > 0) {
                Iterator<String> iterator = params.keySet().iterator();
                while (iterator.hasNext()) {
                    String key = iterator.next();
                    Object obj = params.get(key);
                    if (obj instanceof Long) {
                        query.setLong(key, (Long) obj);
                    } else if (obj instanceof Double) {
                        query.setDouble(key, (Double) obj);
                    } else if (obj instanceof Boolean) {
                        query.setBoolean(key, (Boolean) obj);
                    } else if (obj instanceof String) {
                        query.setString(key, (String) obj);
                    } else if (obj instanceof Date) {
                        query.setTimestamp(key, (Date) obj);
                    } else if (obj.getClass().isEnum()) {
                        query.setLong(key, ((Enum<?>) obj).ordinal());
                    }
                }
            }
            results = query.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return results;
    }

    /**
     * inserts the given entity to the table
     * 
     * @param entity
     *            object to be inserted
     * @return returns the saved object
     * @throws HarbingerException
     *             if there is error in saving the entity
     */
    public Object insertDetails(Object entity) throws HarbingerException {
        Object existingObj = prepareObject(entity);
        if( existingObj != null) { //The object that exists in DB is exactly same as this object
            return existingObj;
        }
        Session session = null;
        Object obj = null;
        try {
        	session = openSession();
            obj = session.merge(entity);
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            }
        } finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return obj;
    }

   /* *//**
     * partial Save
     * 
     * @param entity
     * @param entityName
     * @return
     * @throws HarbingerException
     *//*
    public Object insertDetails(Object entity, String entityName)
            throws HarbingerException {
        Session session = null;
        Object obj = null;
        try {
            session = openSession();
            obj = session.merge(entityName
                    + PartialSave.PARTIAL_TABLE_APPENDER, entity);
            session.flush();
            session.evict(entity);
            
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return obj;
    }*/

    /*
     * Swati,21-02-2011,To save the object containing Clob property.
     */
    /**
     * saves the given entity to the table.
     * 
     * @param entity
     *            object to be saved
     * @throws HarbingerException
     *             if there is error in saving the entity
     */
    public Object saveDetails(Object entity) throws HarbingerException {
        Object existingObj = prepareObject(entity);
        if( existingObj != null) { //The object that exists in DB is exactly same as this object
            return existingObj;
        }
        Session session = null;
        Object savedObject = null;

        try {
            session = openSession();
            savedObject = session.save(entity);
            session.flush();
            session.evict(entity);
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        } finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return savedObject;
    }

    /**
     * updates the given entity to the table.
     * 
     * @param entity
     *            object to be updated
     * @throws HarbingerException
     *             if there is error in saving the entity
     * @return boolean value signifying object updated or not.
     */
    public boolean updateDetails(Object entity) throws HarbingerException {
        boolean ret = true;
        Session session = null;
        Object existingObj = prepareObject(entity);
        if( existingObj != null) { //The object that exists in DB is exactly same as this object
            return false;
        }

        try {
        	session = openSession();
            session.merge(entity);
            session.flush();
        }catch(StaleObjectStateException ex){
        	ret = false;
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
        	ret = false;
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        } finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return ret;
    }

    /**
     * delete's the given entity from the table.
     * 
     * @param entity
     *            object to be deleted.
     * @throws HarbingerException
     *             if it deletion failed
     */
    public void deleteEntity(Object entity) throws HarbingerException {
        DateTimeZone zone = DateTimeZone.getDefault();
        String username = getUserName();
        Session session = openSession();
        try {
            /*if (entity instanceof IAuditLog) {
                IAuditLog BaseEntity = (IAuditLog) entity;
                Date date= new Date();
                long utc = zone.convertLocalToUTC(date.getTime(), false);
                date=new Date(utc);
                BaseEntity.setModified(date);
                baseEntity.setModifiedBy(username);
            }*/
            session.delete(entity);
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else {
            	throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            closeSession(session);
        }
    }

    public void deleteEntity(Object entity, String entityName)
            throws HarbingerException {
        DateTimeZone zone = DateTimeZone.getDefault();
        String username = getUserName();
        Session session = openSession();
        try {
           /* if (entity instanceof IAuditLog) {
                IAuditLog auditLog = (IAuditLog) entity;
                Date date= new Date();
                long utc = zone.convertLocalToUTC(date.getTime(), false);
                date=new Date(utc);
                auditLog.setModified(date);
                auditLog.setModifiedBy(username);
            }*/
            session.delete(entity);
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            closeSession(session);
        }
    }

    /**
     * makes the object disable and don't allow us to access that object.
     * 
     * @param entity
     *            object to be deleted.
     * @return boolean value signifying update is successful or not
     * @throws HarbingerException
     *             throws this exception if in case something happens during
     *             deletion
     */

    public boolean deleteLogical(BaseEntity entity)
            throws HarbingerException {
        DateTimeZone zone = DateTimeZone.getDefault();
        String username = getUserName();
    	boolean ret = true;
        Session session = openSession();
        try {
           /* if (entity instanceof IAuditLog) {
                IAuditLog auditLog = (IAuditLog) entity;
                Date date= new Date();
                long utc = zone.convertLocalToUTC(date.getTime(), false);
                date=new Date(utc);
                auditLog.setModified(date);
                auditLog.setModifiedBy(username);
            }*/
            entity.setEnabled(false);
            BaseEntity attachedEntity = (BaseEntity) session.get(
                    entity.getClass(), new Long(entity.getId()));
            if (attachedEntity == null) {
                return false;
            }
            attachedEntity.setEnabled(false);
            session.saveOrUpdate(attachedEntity);
            session.flush();
        }catch(StaleObjectStateException ex){
        	ret = false;
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
        	ret = false;
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
       }
        return ret;
    }

    /**
     * inserts the list of entities to the table with transaction support
     * 
     * @param entities
     *            list of entities to be inserted
     * 
     * @throws HarbingerException
     *             if insertion failed
     */
    public void insertDetailsWithTransaction(List<?> entities)
            throws HarbingerException {
        Session session = openSession();
        try {
            for (Object entity : entities) {
                validateEntity(entity);
            }
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
    }

    /**
     * 
     * @param entity
     *            object to be validated
     * @return validated object object with some of its
     * @throws HarbingerException
     *             if there is error in validating an entity
     * 
     */
    Object validateEntity(Object entity) throws HarbingerException {
        DateTimeZone zone = DateTimeZone.getDefault();
        BaseEntity baseEntity = null;
        try {

            if (entity instanceof BaseEntity) {
                baseEntity = (BaseEntity) entity;
                String username = getUserName();
                Date date= new Date();
                long utc = zone.convertLocalToUTC(date.getTime(), false);
                date=new Date(utc);
                if (baseEntity.getId() == 0) {
                    baseEntity.setEntered(date);
                    baseEntity.setEnteredBy(username);
                }

                baseEntity.setModified(date);
                baseEntity.setModifiedBy(username);
                //baseEntity.setVersion(baseEntity.getVersion() + 1);
            }
            return baseEntity;
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }

    }


    /**
     * inserts the list of entities to the table.
     * 
     * @param entities
     *            list of entities to be inserted
     * @throws HarbingerException
     *             if there is error in insertion
     */
    public void insertDetailsWithList(List<?> entities)
            throws HarbingerException {
        Session session = openSession();
        try {
            for (Object entity : entities) {
                session.save(entity);
            }
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
    }

    /**
     * @param queryName
     *            name of the named query
     * @param params
     *            named parameters for the query
     * @param maxLimit
     *            no of rows to be fetched
     * @return fetches values for the specified entity using specified named
     *         query and specified limit
     * @throws HarbingerException
     *             if there is error in searching
     */
    public List<?> searchByNamedQueryWithLimit(String queryName,
            Map<String, Object> params, int maxLimit)
            throws HarbingerException {
        Session session = openSession();
        List<?> list = null;
        try {
            final Query query = session.getNamedQuery(queryName);
            query.setMaxResults(maxLimit);
            Iterator<String> iterator = params.keySet().iterator();
            while (iterator.hasNext()) {
                String key = iterator.next();
                Object obj = params.get(key);
                if (obj instanceof Long) {
                    query.setLong(key, (Long) obj);
                } else if (obj instanceof Boolean) {
                    query.setBoolean(key, (Boolean) obj);
                } else if (obj instanceof String) {
                    query.setString(key, (String) obj);
                } else if (obj instanceof Date) {
                    query.setTimestamp(key, (Date) obj);
                } else if (obj.getClass().isEnum()) {
                    query.setLong(key, ((Enum<?>) obj).ordinal());
                }
            }
            list = query.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return list;
    }

    /**
     * @param entityName
     *            fully qualified class of name of the entity
     * @param retrieveAttribute
     *            comma separated string containing attributes that has to be
     *            fetched
     * @param searchAttribute
     *            name of the attribute on which condition has to be applied
     * @param attributeValue
     *            value of the searchAttribute
     * @return return the entities fetched by the following query select
     *         retrieveAttribute from entityName where searchAttribute like
     *         'attributeValue%'
     * @throws HarbingerException
     *             if there is error in searching details
     */
    public List<?> searchDetailsLike(String retrieveAttribute,
            String entityName, String searchAttribute, String attributeValue)
            throws HarbingerException {
        Session session = openSession();
        List<?> list = null;
        try {
            StringBuffer sb = new StringBuffer(" ");
            sb.append(" select " + retrieveAttribute + " " + FROM + " ");
            sb.append(entityName);
            sb.append(" " + AS_ENTITY + " ");
            sb.append(" " + WHERE + " ");
            sb.append(searchAttribute);
            sb.append(" like '");
            sb.append((new StringBuilder(String.valueOf(attributeValue)))
                    .append("%'").toString());
            Query q = session.createQuery(sb.toString());
            list = q.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return list;
    }

    /**
     * inserts the list of entities to the table with transaction support and
     * validation
     * 
     * @param entities
     *            list of entities to be inserted
     * @return returns list of saved entities
     * @throws HarbingerException
     *             if there is error in insertion of list of entities
     */
    public List<?> insertDetailsListWithTransaction(List<?> entities)
            throws HarbingerException {
        List<Object> persistenceEntities = new ArrayList<Object>();
        Session session = openSession();
        try {
            for (Object entity : entities) {
                entity = validateEntity(entity);
                entity = session.merge(entity);
                persistenceEntities.add(entity);
            }
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return persistenceEntities;

    }

    /**
     * Deletes the list of entities from the table.
     * 
     * @param entities
     *            list of entities to be inserted
     * @throws HarbingerException
     *             if there is error in deletion of list of entities
     */
    public void deleteEntityWithTransaction(List<?> entities)
            throws HarbingerException {
        DateTimeZone zone = DateTimeZone.getDefault();
        String username = getUserName();
        Session session = openSession();
        try {
            for (Object entity : entities) {
                if (entity instanceof BaseEntity) {
                    BaseEntity baseEntity = (BaseEntity) entity;
                    Date date= new Date();
                    long utc = zone.convertLocalToUTC(date.getTime(), false);
                    date=new Date(utc);
                    baseEntity.setModified(date);
                    baseEntity.setModifiedBy(username);
                }
                session.delete(entity);
            }
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }

    }

    /**
     * @param entityName
     *            fully qualified class of name of the entity
     * @param selectItems
     *            comma separated string containing attributes that has to be
     *            fetched
     * @param condition
     *            condition for the query
     * @return return the entities fetched by the following query select
     *         selectItems from entityName where condition
     * @throws HarbingerException
     *             if there is error in searching
     */
    public List<?> searchByCondition(String selectItems, String entityName,
            String condition) throws HarbingerException {
        Session session = openSession();
        List<?> list = null;
        try {
            StringBuffer sb = new StringBuffer(" ");
            sb.append("select ");
            sb.append(selectItems);
            sb.append(" " + FROM + " ");
            sb.append(entityName);
            if (condition != null) {
                sb.append(" " + WHERE + " ");
                sb.append(condition);
            }
            Query query = session.createQuery(sb.toString());
            list = query.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return list;
    }

    /***
     * 
     * @author Monika
     * @param entityName
     *            fully qualified class name of the entity
     * @param condition
     *            which will be used in queries to select the objects
     * @param pageIndex
     *            should be > 0, first page will have the index 1
     * @param pageSize
     *            can be 1,2,3, indicates number of results to be fetched per page
     * @return list of objects starting from page index till the number of
     *         objects
     * @throws HarbingerException
     *             if there any error in searching the entity
     * 
     */
    public List<?> searchByConditionWithPagination(String entityName,
            String condition, int pageIndex, int pageSize)
            throws HarbingerException {
        Session session = openSession();
        List<?> list = null;
        try {
            StringBuffer sb = new StringBuffer(FROM + " ");
            sb.append(entityName);
            if (condition != null && condition.length() > 0) {
                sb.append(" " + WHERE + " ");
                sb.append(condition);
            }
            Query query = session.createQuery(sb.toString());
            query.setFirstResult((pageIndex - 1) * pageSize);
            query.setMaxResults(pageSize);
            list = query.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return list;
    }
    
    /***
     * 
     * @author Rose
     * @param entityName
     *            fully qualified class name of the entity
     * @param condition
     *            which will be used in queries to select the objects
     * @param firstResult
     *            the index of the first result you want to be fetched
     * @param pageSize
     *            can be 1,2,3, indicates number of results to fetched per page
     * @return list of objects starting from firstResult till the number of
     *         objects
     * @throws HarbingerException
     *             if there any error in searching the entity
     * 
     */
    public List<?> searchByConditionPaginatedWithCustomFirstResult(String entityName,
            String condition, int firstResult, int pageSize)
            throws HarbingerException {
        Session session = openSession();
        List<?> list = null;
        try {
            StringBuffer sb = new StringBuffer(FROM + " ");
            sb.append(entityName);
            if (condition != null && condition.length() > 0) {
                sb.append(" " + WHERE + " ");
                sb.append(condition);
            }
            Query query = session.createQuery(sb.toString());
            query.setFirstResult(firstResult);
            query.setMaxResults(pageSize);
            list = query.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return list;
    }

    /**
     * @param entityName
     *            fully qualified class name of the entity
     * @param searchAttribute
     *            specifies which entity should be searched in a entity through
     *            query
     * @return return's list of values which matches search attributes
     * @throws HarbingerException
     *             if there is error in searching for entity
     */
    public List<?> searchDetailsBySearchAttributes(String entityName,
            String searchAttribute) throws HarbingerException {
        Session session = openSession();
        List<?> list = null;
        try {
            StringBuffer sb = new StringBuffer(" ");
            sb.append(" " + FROM + " ");
            sb.append(entityName);
            sb.append(" " + AS_ENTITY + " ");
            sb.append(" " + WHERE + " ");
            sb.append(searchAttribute);
            Query query = session.createQuery(sb.toString());
            list = query.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return list;
    }

    /**
     * @param entityName
     *            fully qualified class name of the entity
     * @param searchAttribute
     *            specifies which entity should be searched in a entity through
     *            query
     * @param limit
     *            no of rows to be fetched from the list of entities which
     *            matches search attributes
     * @return return's list of values
     * @throws HarbingerException
     *             if there is error in searching list of entities
     */
    public List<?> searchDetailsBySearchAttributesWithLimts(String entityName,
            String searchAttribute, int limit) throws HarbingerException {
        Session session = openSession();
        List<?> list = null;
        try {
            StringBuffer sb = new StringBuffer(" ");
            sb.append(" " + FROM + " ");
            sb.append(entityName);
            sb.append(" " + AS_ENTITY + " ");
            sb.append(" " + WHERE + " ");
            sb.append(searchAttribute);
            Query query = session.createQuery(sb.toString());
            query.setMaxResults(limit);
            list = query.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return list;
    }

    /**
     * @param queryName
     *            name of the named query
     * @param params
     *            named parameters for the specified named query
     * @param startIndex
     *            indexed fetching
     * @param offset
     *            how many has to be fetched ?
     * @return fetches entities from index till index+offset
     * @throws HarbingerException
     *             if there is error in searching by named query
     */
    public List<?> searchByNamedQuery(String queryName,
            Map<String, Object> params, int startIndex, int offset)
            throws HarbingerException {
        Session session = null;
        List<?> list = null;
        try {
            session = openSession();
            Query query = session.getNamedQuery(queryName);
            query.setFirstResult(startIndex);
            query.setMaxResults(offset);
            Iterator<String> iterator = params.keySet().iterator();
            while (iterator.hasNext()) {
                String key = iterator.next();
                Object obj = params.get(key);
                if (obj instanceof Long) {
                    query.setLong(key, (Long) obj);
                } else if (obj instanceof Double) {
                    query.setDouble(key, (Double) obj);
                } else if (obj instanceof Boolean) {
                    query.setBoolean(key, (Boolean) obj);
                } else if (obj instanceof String) {
                    query.setString(key, (String) obj);
                } else if (obj instanceof Date) {
                    query.setTimestamp(key, (Date) obj);
                } else if (obj.getClass().isEnum()) {
                    query.setLong(key, ((Enum<?>) obj).ordinal());
                }
            }
            list = query.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return list;
    }

    /**
     * @param myClass
     *            full qualified class name of entity
     * @return no of rows available in table for specified entity
     * @throws HarbingerException
     *             if any error in getting the rows
     */
    public long getRowsCount(Class myClass) throws HarbingerException {
    	Session session = null;
        long rowCount = 0;
        try {
            session = openSession();
            Criteria criteria = session.createCriteria(myClass);
            criteria.setProjection(Projections.rowCount());
            Long count = (Long) criteria.uniqueResult();
            rowCount = count.intValue();
            criteria.setProjection(null);
            criteria.setResultTransformer(Criteria.ROOT_ENTITY);
           
        } catch (HibernateException t) {
            throw new HarbingerException(CommonError.QUERY_ERROR,
                    t.getMessage(), t);
        } finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return rowCount;
    }

    /**
     * @param entityName
     *            fully qualified class name of entity
     * @param entityObj
     *            criteria object
     * @return returns list of entities matching criteria
     * @throws HarbingerException
     *             if there is error in searching an entity based on criteria
     */
    public List<?> searchDetailsByCriteria(String entityName, Object entityObj)
            throws HarbingerException {
        Session session = null;
        List<?> list = null;
        try {
            session = openSession();
            Class reqClass = Class.forName(entityName);
            Criteria criteria = session.createCriteria(reqClass);
            java.lang.reflect.Field[] fields = reqClass.getDeclaredFields();
            for (java.lang.reflect.Field field : fields) {
                field.setAccessible(true);
                Object obj = field.get(entityObj);

                if (obj != null && !obj.toString().equals("0")
                        && !obj.toString().equals("")
                        && !obj.toString().equals("0.0")) {

                    criteria.add(Expression.eq(field.getName(), obj));
                }
            }
            list = criteria.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }catch (ClassNotFoundException ex) {
            throw new HarbingerException(
                    CommonError.CLASS_NOT_FOUND_ERROR, ex.getMessage(), ex);
        } catch (IllegalAccessException ex) {
            throw new HarbingerException(
                    CommonError.ILLEGAL_ACCESS_ERROR, ex.getMessage(), ex);
        } finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return list;
    }

    /**
     * @param entityName
     *            fully qualified class name of entity
     * @param entityObj
     *            criteria object
     * @return returns list of entities matching criteria
     * @throws HarbingerException
     *             if there is error in searching an entity with custom criteria
     */
    public List<?> searchDetailsByCustomCriteriaClass(String entityName,
            Object entityObj) throws HarbingerException {
        Session session = null;
        List<?> list = null;
        try {
            session = openSession();
            Class reqClass = Class.forName(entityName);
            Criteria criteria = session.createCriteria(reqClass);
            java.lang.reflect.Field[] fields = reqClass.getDeclaredFields();
            for (java.lang.reflect.Field field : fields) {
                field.setAccessible(true);
                Object obj = field.get(entityObj);

                if (obj != null && (!obj.toString().equals(""))) {

                    criteria.add(Expression.eq(field.getName(), obj));
                }
            }
            list = criteria.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }catch (ClassNotFoundException ex) {
            throw new HarbingerException(
                    CommonError.CLASS_NOT_FOUND_ERROR, ex.getMessage(), ex);
        } catch (IllegalAccessException ex) {
            throw new HarbingerException(
                    CommonError.ILLEGAL_ACCESS_ERROR, ex.getMessage(), ex);
        } finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return list;
    }

    /**
     * @param qry
     *            hql query to be executed
     * @return returns list of entities returned by hql query
     * @throws HarbingerException
     *             if there is error in query
     */
    public List<?> executeCustomQuery(String qry)
            throws HarbingerException {
        Session session = null;
        List<?> list = null;
        try {
            session = openSession();
            Query query = session.createQuery(qry);
            list= query.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else{
            throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return list;
    }
    
    /**
     * Execute Custom Query (UPDATE /DELETE) with parameter
     * 
     * @param queryName
     *            name of the named query
     * @param params
     *            named parameters for specified query
     * @return Rows affected
     * @throws HarbingerException
     *             if there is error in execution of query
     */
    public long executeNamedQuery(String queryName, Map<String, Object> params)
            throws HarbingerException {
    	Session session = null;
    	long rowsAffected = 0;
    	try { 
    		session = openSession();
    		Query query = session.getNamedQuery(queryName);

    		if (params != null && params.size() > 0) {
    			Iterator<String> iterator = params.keySet().iterator();
    			while (iterator.hasNext()) {
    				String key = iterator.next();
    				Object obj = params.get(key);
    				if (obj instanceof Long) {
    					query.setLong(key, (Long) obj);
    				} else if (obj instanceof Double) {
    					query.setDouble(key, (Double) obj);
    				} else if (obj instanceof Boolean) {
    					query.setBoolean(key, (Boolean) obj);
    				} else if (obj instanceof String) {
    					query.setString(key, (String) obj);
    				} else if (obj instanceof Date) {
    					query.setTimestamp(key, (Date) obj);
    				} else if (obj.getClass().isEnum()) {
    					query.setLong(key, ((Enum<?>) obj).ordinal());
    				}
    			}
    		}
            rowsAffected = (long) query.executeUpdate();
            session.flush();
        } catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else{
            	throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                    ex.getMessage(), ex);
            } 
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return rowsAffected;
    }
    
    /***
     * 
     * @author Rose
     * @param qry
     *            a full hql query to be executed, includes entityName and condition
     * @param pageIndex
     *            should be greater than 0, starts from 1. Used to compute from which
     *            page onwards the results should be fetched
     * @param pageSize
     *            can be 1,2,3, indicates number of results to fetched per page
     * @return list of objects starting from pageIndex till the number of
     *         objects returned after query execution
     * @throws HarbingerException
     *             if there any error in searching the entity
     * 
     */

    public List<?> executeCustomQueryWithPagination(String qry, int pageIndex, int pageSize)
            throws HarbingerException {
        Session session = null;
        List<?> list = null;
        try {
            session = openSession();
            Query query = session.createQuery(qry);
            query.setFirstResult((pageIndex - 1) * pageSize);
            query.setMaxResults(pageSize);
            list = query.list();
        } catch (StaleObjectStateException ex) {
            throw new HarbingerException(
                    CommonError.CONCURRENT_MODIFICATION_ERROR, ex.getMessage(),
                    ex);
        } catch (HibernateException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throw new HarbingerException(
                        CommonError.CONSTRAINT_VIOLATION_ERROR,
                        ex.getMessage(), ex);
            } else if (ex.getCause() instanceof DataException) {
                throw new HarbingerException(CommonError.DATA_EXCEPTION,
                        ex.getMessage(), ex);
            } else if (ex.getCause() instanceof JDBCConnectionException) {
                throw new HarbingerException(
                        CommonError.JDBC_CONNECTION_EXCEPTION, ex.getMessage(),
                        ex);
            } else if (ex.getCause() instanceof SQLGrammarException) {
                throw new HarbingerException(
                        CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,
                        ex.getMessage(), ex);
            } else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            } else {
                throw new HarbingerException(
                        CommonError.DB_SESSION_ERROR, ex.getMessage(), ex);
            }
        } finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return list;
    }

    public List<?> executeCustomQueryWithLimit(String qry, int limit) throws HarbingerException {
    	Session session = null;
    	List<?> list = null;
    	try {
    		session = openSession();
    		Query query = session.createQuery(qry);
    		query.setMaxResults(limit);
    		list = query.list();
    	} catch (StaleObjectStateException ex) {
    		throw new HarbingerException(
    				CommonError.CONCURRENT_MODIFICATION_ERROR, ex.getMessage(),
    				ex);
    	} catch (HibernateException ex) {
    		if (ex.getCause() instanceof ConstraintViolationException) {
    			throw new HarbingerException(
    					CommonError.CONSTRAINT_VIOLATION_ERROR,
    					ex.getMessage(), ex);
    		} else if (ex.getCause() instanceof DataException) {
    			throw new HarbingerException(CommonError.DATA_EXCEPTION,
    					ex.getMessage(), ex);
    		} else if (ex.getCause() instanceof JDBCConnectionException) {
    			throw new HarbingerException(
    					CommonError.JDBC_CONNECTION_EXCEPTION, ex.getMessage(),
    					ex);
    		} else if (ex.getCause() instanceof SQLGrammarException) {
    			throw new HarbingerException(
    					CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,
    					ex.getMessage(), ex);
    		}  else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            } else {
    			throw new HarbingerException(
    					CommonError.DB_SESSION_ERROR, ex.getMessage(), ex);
    		}
    	} finally {
    		if (session != null) {
    			closeSession(session);
    		}
    	}
    	return list;
    }

    public List<?> executeCustomSQLQuery(String qry, int resultSize)
            throws HarbingerException {
        Session session = null;
        List<?> list = null;
        try {
            session = openSession();
            SQLQuery query = session.createSQLQuery(qry);
            if(resultSize !=0 ) {
                query.setMaxResults(resultSize);
            }
            list= query.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else{
                throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                        ex.getMessage(), ex);
            }
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return list;
    }

    public List<?> executeCustomSQLQuery(String qry)
            throws HarbingerException {
        List list = executeCustomSQLQuery(qry,0);
        return list;
    }



    String getUserName() {
    	String userName = UserName.getUserName();
    	if (userName == null || userName.isEmpty()) {
    		userName = DEFAULT_USERNAME;
    	}
    	return userName;
    }
    
    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * @param entityName
     *            the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

    private Object prepareObject (Object entity) throws HarbingerException {
        DateTimeZone zone = DateTimeZone.getDefault();
        Object existingObject = null;
        Date date= new Date();
        long utc = zone.convertLocalToUTC(date.getTime(), false);
        date=new Date(utc);
        if (entity instanceof BaseEntity) {
            BaseEntity baseEntity = (BaseEntity) entity;
            if (baseEntity.getId() > 0) {
                long id = baseEntity.getId();
                existingObject = searchDetails(entity.getClass()
                        .getCanonicalName(), id);
                if (existingObject != null) {
                    boolean changed = /*new ObjectUtil().isEqual(entity,
                            existingObject);*/false;
                    if (!changed) {
                        return existingObject;
                    }
                    if(baseEntity.getVersion() == 0)
                        baseEntity.setVersion(((BaseEntity)existingObject).getVersion());
                }
            }

            if (existingObject == null) {
                baseEntity.setVersion(1);
                baseEntity.setEntered(date);
                if(baseEntity.getEnteredBy()==null || baseEntity.getEnteredBy().isEmpty()){
                    baseEntity.setEnteredBy(getUserName());
                }
            } else {
             //   if (baseEntity.getEntered() == null) {
                    baseEntity.setEntered(((BaseEntity) existingObject)
                            .getEntered());
                    baseEntity.setEnteredBy(((BaseEntity) existingObject)
                            .getEnteredBy());
               // }
            }
            baseEntity.setModified(date);
            baseEntity.setModifiedBy(getUserName());
        } /*else if( entity instanceof IAuditLog) {
            if(!setDefaultVersion(entity)) {
                return entity;
            }
            IAuditLog ent = (IAuditLog) entity;
            ent.setModified(date);
            ent.setModifiedBy(getUserName());
        } */else { //special handling for non named entity
            if(!setDefaultVersion(entity)) {
                return entity;
            }
        }
        return null;
    }
	
	private boolean setDefaultVersion(Object entity) {
		long id = 0, version = 0, existingVersion=0;
		Object existingObject = null;
		try {
			// get the getter and setter methods for id and version
			Method methods[] = ReflectionUtils.getAllDeclaredMethods(entity
					.getClass());
			Method getIdMethod = null, setVersionMethod = null, getVersionMethod =null;
			for (int i = 0; i < methods.length; i++) {
				if ("getId".equals(methods[i].getName())) {
					getIdMethod = methods[i];
				}
				if ("setVersion".equals(methods[i].getName())) {
					setVersionMethod = methods[i];
				}
				if ("getVersion".equals(methods[i].getName())) {
					getVersionMethod = methods[i];
				}
			}
			if (setVersionMethod != null) {
				if (getIdMethod != null) {
					id = (Long) ReflectionUtils.invokeMethod(getIdMethod, entity);
				}
				if (id > 0) { // if id>0 then check if row exists in db, if so set the old version
					existingObject = searchDetails(entity.getClass().getCanonicalName(), id);
					if (existingObject != null) {
						boolean changed = /*new ObjectUtil().isEqual(entity, existingObject);*/false;
                	    if (!changed) {
                	    	entity = existingObject;
                	    	return false;
                	    }
                	    if(getVersionMethod != null) {
                	    	version = (Long) ReflectionUtils.invokeMethod(getVersionMethod, entity);
                	    	existingVersion = (Long) ReflectionUtils.invokeMethod(getVersionMethod, existingObject);
                	    	if(version==0) {
                	    			ReflectionUtils.invokeMethod(setVersionMethod, entity, existingVersion);
                	    			return true;
                	    	}
                	    }
					}
				} 
				if(existingObject == null) { // if row does not exist in db, it is being created for 1st time, explicitly set version to 1
						ReflectionUtils.invokeMethod(setVersionMethod, entity, 1);
						return true;
				}
			}
		} catch (Exception e) {
			// do nothing
		}
		return true;
	}

    /* Generic APIs */
    public List<?> search(String entityName)
            throws HarbingerException {
        Session session = openSession();
        List<?> l = null;
        try {
            StringBuffer sb = new StringBuffer(FROM + " ");
            sb.append(entityName);
            sb.append(" " + AS_ENTITY + " ");

            Query q = null;
            q = session.createQuery(sb.toString());

            l = q.list();
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
                throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                        ex.getMessage(), ex);
            }
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return l;
    }

    public Object insert(Object entity) throws HarbingerException {
        Session session = null;
        Object obj = null;
        try {
            session = openSession();
            obj = session.merge(entity);
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
            	 throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else{
                throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                        ex.getMessage(), ex);
            }
        } finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return obj;
    }

    /* UUID APIs */

    /*public Object searchObjectWithUUID(String entityName, BigInteger id)
            throws HarbingerException {
        Session session = openSession();
        Object obj = null;
        try {
            StringBuffer sb = new StringBuffer(FROM + " ");
            sb.append(entityName);
            sb.append(" " + AS_ENTITY + " where entity.id = ");
            sb.append(id);
            Query q = session.createQuery(sb.toString());
            List<? extends BaseEntity> l = q.list();
            if (l !=null && l.size() > 0) {
                obj = l.get(0);
            }
            session.flush();
        }catch(StaleObjectStateException ex){
            throw new HarbingerException(CommonError.CONCURRENT_MODIFICATION_ERROR,ex.getMessage(),ex);
        }catch (HibernateException ex) {
            if(ex.getCause() instanceof ConstraintViolationException){
                throw new HarbingerException(CommonError.CONSTRAINT_VIOLATION_ERROR,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof DataException){
                throw new HarbingerException(CommonError.DATA_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof JDBCConnectionException){
                throw new HarbingerException(CommonError.JDBC_CONNECTION_EXCEPTION,ex.getMessage(),ex);
            }else if(ex.getCause() instanceof SQLGrammarException){
                throw new HarbingerException(CommonError.FAILED_TO_EXECUTE_SQL_STATEMENT,ex.getMessage(),ex);
            }else{
                throw new HarbingerException(CommonError.DB_SESSION_ERROR,
                        ex.getMessage(), ex);
            }
        }finally {
            if (session != null) {
                closeSession(session);
            }
        }
        return obj;
    }*/
}