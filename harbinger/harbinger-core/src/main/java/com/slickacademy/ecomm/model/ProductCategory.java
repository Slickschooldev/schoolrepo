package com.slickacademy.ecomm.model;

import com.slickacademy.db.util.BaseEntity;

/**
 * Created by hemanth on 15/5/16.
 */
public class ProductCategory extends BaseEntity {
    private String bannerImageUrl;

    public void setBannerImageUrl(String bannerImageUrl) {
        this.bannerImageUrl = bannerImageUrl;
    }

    public String getBannerImageUrl() {
        return bannerImageUrl;
    }
}
