package com.slickacademy.context;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Created by ganesh on 3/6/15.
 */
public class ApplicationContextProvider implements ApplicationContextAware{
    private static ApplicationContext applicationContext;
    @Override
    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        applicationContext=ctx;
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}
