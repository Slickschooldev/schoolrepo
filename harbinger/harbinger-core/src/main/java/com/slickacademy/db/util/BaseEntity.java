/**
 * 
 */
package com.slickacademy.db.util;

import java.util.Date;


/**
 * @author dev
 *
 */

public class BaseEntity {



	private long id;
	private String modifiedBy;
	private Date modified;
	private long version;
	private String name;
	private String description;

	private String enteredBy;
	private Date entered;
	private boolean enabled=true;



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}
	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public BaseEntity(int id, String name) {
		this.id = id ;
		this.name = name;
	}

	public BaseEntity(String name ) {
		this.name = name;
	}

	public BaseEntity() {
		this.name = "";
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the enteredBy
	 */
	public String getEnteredBy() {
		return enteredBy;
	}

	/**
	 * @param enteredBy the enteredBy to set
	 */
	public void setEnteredBy(String enteredBy) {
		this.enteredBy = enteredBy;
	}

	/**
	 * @return the entered
	 */
	public Date getEntered() {
		return entered;
	}

	/**
	 * @param entered the entered to set
	 */
	public void setEntered(Date entered) {
		this.entered = entered;
	}

	public String toString() {
		return this.getClass().getName()+ "[id=" + this.getId() + ", name="+ getName()+ "]";
	}

    /*@Override
    public String getLogDetails() {
    	String result = "";
    	if(this.getName()!=null && !this.getName().equals(""))
    		result += "<b>Name:</b>" + this.getName();
    	if(this.getDescription()!=null && !this.getDescription().equals(""))
    		result +=" <b>Description:</b>" + this.getDescription();
    	return result;
    }*/
    
    /*@Override
    public String getUpdateDetails(UpdateDetails updateDetails) {
        StringBuffer sb = new StringBuffer("");
    	for(int i=0; i<updateDetails.propertyNames.length; i++) {
    		if(!updateDetails.propertyNames[i].equals("version") && 
    				!updateDetails.propertyNames[i].equals("entered")&& !updateDetails.propertyNames[i].equals("enteredBy") &&
    				!updateDetails.propertyNames[i].equals("modified") && !updateDetails.propertyNames[i].equals("modifiedBy")) {
    			*//*if(this instanceof ITransaction) {
    				if(updateDetails.propertyNames[i].equals("transactionId"))
    					continue;
    			}*//*
    			if(updateDetails.previousState[i]==null && (updateDetails.currentState[i]!=null && updateDetails.currentState[i].equals(""))) {
    				continue;
    			} 
    			if(updateDetails.previousState[i]!=null && updateDetails.previousState[i] instanceof BaseEntity) {
    				sb.append("Field <b>'" + updateDetails.propertyNames[i]+"'</b> was modified from '" + ((updateDetails.previousState[i]!=null)?
    						((BaseEntity) updateDetails.previousState[i]).getName():"") + "' to <b>'" + ((updateDetails.currentState[i]!=null)?((BaseEntity)updateDetails.currentState[i]).getName():"") + "'</b></br>");
    			} else {
                    if(isReferencedEntity(updateDetails.previousState[i])) {
                       sb.append("Field <b>'" + updateDetails.propertyNames[i] + "'</b> was modified.");
                    } else {
                        sb.append("Field <b>'" + updateDetails.propertyNames[i] + "'</b> was modified from '" +
                                updateDetails.previousState[i] + "' to <b>'" + updateDetails.currentState[i] + "'</b></br>");
                    }
    			}
    		}
    	}
    	return sb.toString();
    }*/
    
    public String stringifyObject() {
    	StringBuffer stringifiedObject = new StringBuffer();
    	stringifiedObject.append("Id: "+getId()+"<br>");
    	String attribute = getName();
    	stringifiedObject.append("Name: "+(attribute==null?"...":getTruncatedAttributeVal(attribute))+"<br>");
    	attribute = getDescription();
    	stringifiedObject.append("Description: "+(attribute==null?"...":getTruncatedAttributeVal(attribute))+"<br>");
    	return stringifiedObject.toString();
    }

    private String getTruncatedAttributeVal(String attribute) {
    	if(attribute!=null && !attribute.isEmpty()) {
    		if(attribute.length()>15) {
    			attribute = attribute.substring(0,13);
    			attribute += "...";
    		}
    	}
    	return attribute;
    }
}
