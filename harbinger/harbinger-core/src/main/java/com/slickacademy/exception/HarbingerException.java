package com.slickacademy.exception;

/**
 * Created by ganesh on 29/5/15.
 */
public class HarbingerException extends Exception {
    IError error;

    public HarbingerException(IError error, String message, Throwable throwable) {
        super(message, throwable);
        this.error =error;
    }

    public HarbingerException(IError error, String message) {
        super(message);
        this.error =error;
    }

    public IError getError() {
        return error;
    }
}
