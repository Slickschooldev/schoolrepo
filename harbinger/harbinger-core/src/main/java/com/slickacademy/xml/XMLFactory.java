package com.slickacademy.xml;

import com.slickacademy.db.util.Manager;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;

public class XMLFactory extends Manager{
	private static Logger logger = Logger.getLogger(XMLFactory.class);


	private BasicConfiguration basicConfiguration = null;

	public Object getContentObject(String name) {

		if(name == null){
			return null;
		}
		char dot= '.';
		int lastDotPosition = name.lastIndexOf(dot);
		if(lastDotPosition <=0) {			
			return null;
		}
		String packageName = name.substring(0,lastDotPosition);
		String className = name.substring(lastDotPosition);
		if(packageName == null || className == null) {			
			return null;
		}      
		String applicationPath = null;
		try {
			applicationPath = getApplicationContext().getResource("/").getFile().getAbsolutePath();			
			applicationPath = applicationPath.replace("\\", "/");
			if (!applicationPath.endsWith("/")) {
				applicationPath+="/";
			}
		} catch (IOException e) {
			logger.error(e);
		}		
		/**
		 * Basic Configuration doesn't take any fileName, following code might create problem in future
		 */
		return getContentObject(packageName,applicationPath+getBasicConfiguration().getValue(name));    
	}

	public Object getContentObject(String packageName, String fileName) {
		try {
			JAXBContext jc = JAXBContext.newInstance(packageName);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			logger.debug("Unmarshallling for "+ packageName + " from "+ fileName);
			return unmarshaller.unmarshal(new File(fileName));
		} catch (Exception ex) {
			logger.error(ex);
		}
		return null;
	}
	/**
	 * @return the basicConfiguration
	 */
	public BasicConfiguration getBasicConfiguration() {
		return basicConfiguration;
	}

	/**
	 * @param basicConfiguration the basicConfiguration to set
	 */
	public void setBasicConfiguration(BasicConfiguration basicConfiguration) {
		this.basicConfiguration = basicConfiguration;
	}

	@Override
	public void init() {
	
	}
}
