package com.slickacademy.exception;


public interface IError {
    public String getErrorCode();
    public String getDescription();
    public SEVERITY getSeverity();
    public String getHint();
}