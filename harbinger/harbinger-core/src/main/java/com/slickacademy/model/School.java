package com.slickacademy.model;

import com.slickacademy.db.util.BaseEntity;

/**
 * Created by govind on 30/6/15.
 */
public class School extends BaseEntity {
    private String smsCode;
    private String website;
    private String address;
    private String phoneNumber;

    public String getSmsCode() {
        return smsCode;
    }

    public void setSmsCode(String smsCode) {
        this.smsCode = smsCode;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
