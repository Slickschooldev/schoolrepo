<%@ page import="com.slickacademy.db.util.PersistenceManager" %>
<%@ page import="com.slickacademy.ecomm.model.Product" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.HashSet" %>
<%@ page import="com.slickacademy.model.Currency" %>
<%@ page import="com.slickacademy.ecomm.model.ProductCategory" %>
<%--
  Created by IntelliJ IDEA.
  User: hemanth
  Date: 4/6/16
  Time: 5:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  PersistenceManager pm = PersistenceManager.getInstance();
  Product product = new Product();
  ProductCategory productCategory = new ProductCategory();
  productCategory.setId(1);
  Set<String> colors = new HashSet<String>();
  colors.add("yellow");
  colors.add("white");
  Set<String> sizes = new HashSet<String>();
  sizes.add("XS");
  sizes.add("S");
  sizes.add("M");
  sizes.add("L");
  sizes.add("XL");
  sizes.add("XXL");
  Set<String> productImageUrls = new HashSet<String>();
  productImageUrls.add("images/hiddenSeam/productTiles/pic1.jpg");
  product.setColors(colors);
  product.setCurrency(Currency.RUPEE);
  product.setDescription("Sample description");
  product.setPrice(213.22);
  product.setProductCategory(productCategory);
  product.setProductImageUrls(productImageUrls);
  product.setSizes(sizes);
  pm.insertDetails(product);


  product = new Product();
  colors = new HashSet<String>();
  colors.add("brown");
  colors.add("red");
  colors.add("pink");
  colors.add("orange");
  sizes = new HashSet<String>();
    sizes.add("XS");
    sizes.add("S");
    sizes.add("M");
    sizes.add("L");
    sizes.add("XL");
    sizes.add("XXL");
  productImageUrls = new HashSet<String>();
  productImageUrls.add("images/hiddenSeam/productTiles/pic2.jpg");
  product.setColors(colors);
  product.setCurrency(Currency.RUPEE);
  product.setDescription("Sample description");
  product.setPrice(220);
  product.setProductCategory(productCategory);
  product.setProductImageUrls(productImageUrls);
  product.setSizes(sizes);
  pm.insertDetails(product);


  product = new Product();
  colors = new HashSet<String>();
  colors.add("yellow");
  colors.add("white");
    colors.add("purple");
    colors.add("black");
    colors.add("brown");
    colors.add("red");
  sizes = new HashSet<String>();
    sizes.add("XS");
    sizes.add("S");
    sizes.add("M");
    sizes.add("L");
    sizes.add("XL");
    sizes.add("XXL");
  productImageUrls = new HashSet<String>();
  productImageUrls.add("testUrl1");
  product.setColors(colors);
  product.setCurrency(Currency.RUPEE);
  product.setDescription("Sample description");
  product.setPrice(300);
  product.setProductCategory(productCategory);
  product.setProductImageUrls(productImageUrls);
  product.setSizes(sizes);
  pm.insertDetails(product);


  product = new Product();
  colors = new HashSet<String>();
  colors.add("yellow");
  colors.add("white");
  colors.add("purple");
  colors.add("black");
  sizes = new HashSet<String>();
    sizes.add("XS");
    sizes.add("S");
    sizes.add("M");
    sizes.add("L");
    sizes.add("XL");
    sizes.add("XXL");
  productImageUrls = new HashSet<String>();
  productImageUrls.add("images/hiddenSeam/productTiles/pic3.jpg");
  product.setColors(colors);
  product.setCurrency(Currency.RUPEE);
  product.setDescription("Sample description");
  product.setPrice(212);
  product.setProductCategory(productCategory);
  product.setProductImageUrls(productImageUrls);
  product.setSizes(sizes);
  pm.insertDetails(product);


  product = new Product();
  colors = new HashSet<String>();
  colors.add("yellow");
  colors.add("white");
  sizes = new HashSet<String>();
    sizes.add("XS");
    sizes.add("S");
    sizes.add("M");
    sizes.add("L");
    sizes.add("XL");
    sizes.add("XXL");
  productImageUrls = new HashSet<String>();
  productImageUrls.add("images/hiddenSeam/productTiles/pic4.jpg");
  product.setColors(colors);
  product.setCurrency(Currency.RUPEE);
  product.setDescription("Sample description");
  product.setPrice(213.22);
  product.setProductCategory(null);
  product.setProductImageUrls(productImageUrls);
  product.setSizes(sizes);
  pm.insertDetails(product);


  productCategory.setId(2);
  product = new Product();
  colors = new HashSet<String>();
  colors.add("yellow");
  colors.add("white");
  sizes = new HashSet<String>();
    sizes.add("XS");
    sizes.add("S");
    sizes.add("M");
    sizes.add("L");
    sizes.add("XL");
    sizes.add("XXL");
  productImageUrls = new HashSet<String>();
  productImageUrls.add("images/hiddenSeam/productTiles/pic5.jpg");
  product.setColors(colors);
  product.setCurrency(Currency.RUPEE);
  product.setDescription("Sample description");
  product.setPrice(213.22);
  product.setProductCategory(productCategory);
  product.setProductImageUrls(productImageUrls);
  product.setSizes(sizes);
  pm.insertDetails(product);


  product = new Product();
  colors = new HashSet<String>();
  colors.add("pink");
  colors.add("white");
  sizes = new HashSet<String>();
    sizes.add("XS");
    sizes.add("S");
    sizes.add("M");
    sizes.add("L");
    sizes.add("XL");
    sizes.add("XXL");
  productImageUrls = new HashSet<String>();
  productImageUrls.add("images/hiddenSeam/productTiles/pic6.jpg");
  product.setColors(colors);
  product.setCurrency(Currency.RUPEE);
  product.setDescription("Sample description");
  product.setPrice(213.22);
  product.setProductCategory(productCategory);
  product.setProductImageUrls(productImageUrls);
  product.setSizes(sizes);
  pm.insertDetails(product);


  product = new Product();
  colors = new HashSet<String>();
  colors.add("yellow");
  colors.add("black");
    colors.add("orange");
    colors.add("red");
  sizes = new HashSet<String>();
    sizes.add("XS");
    sizes.add("S");
    sizes.add("M");
    sizes.add("L");
    sizes.add("XL");
    sizes.add("XXL");
  productImageUrls = new HashSet<String>();
  productImageUrls.add("images/hiddenSeam/productTiles/pic7.jpg");
  product.setColors(colors);
  product.setCurrency(Currency.RUPEE);
  product.setDescription("Sample description");
  product.setPrice(213.22);
  product.setProductCategory(productCategory);
  product.setProductImageUrls(productImageUrls);
  product.setSizes(sizes);
  pm.insertDetails(product);

  product = new Product();
  colors = new HashSet<String>();
  colors.add("yellow");
  colors.add("white");
  sizes = new HashSet<String>();
    sizes.add("XS");
    sizes.add("S");
    sizes.add("M");
    sizes.add("L");
    sizes.add("XL");
    sizes.add("XXL");
  productImageUrls = new HashSet<String>();
  productImageUrls.add("images/hiddenSeam/productTiles/pic8.jpg");
  product.setColors(colors);
  product.setCurrency(Currency.RUPEE);
  product.setDescription("Sample description");
  product.setPrice(213.22);
  product.setProductCategory(productCategory);
  product.setProductImageUrls(productImageUrls);
  product.setSizes(sizes);
  pm.insertDetails(product);

    productCategory.setId(3);
    product = new Product();
    colors = new HashSet<String>();
    colors.add("pink");
    colors.add("red");
    sizes = new HashSet<String>();
    sizes.add("XS");
    sizes.add("S");
    sizes.add("M");
    sizes.add("L");
    sizes.add("XL");
    sizes.add("XXL");
    productImageUrls = new HashSet<String>();
    productImageUrls.add("images/hiddenSeam/productTiles/pic4.jpg");
    product.setColors(colors);
    product.setCurrency(Currency.RUPEE);
    product.setDescription("Sample description");
    product.setPrice(2134);
    product.setProductCategory(productCategory);
    product.setProductImageUrls(productImageUrls);
    product.setSizes(sizes);
    pm.insertDetails(product);


    product = new Product();
    colors = new HashSet<String>();
    colors.add("brown");
    colors.add("red");
    colors.add("pink");
    sizes = new HashSet<String>();
    sizes.add("XS");
    sizes.add("S");
    sizes.add("M");
    sizes.add("L");
    sizes.add("XL");
    sizes.add("XXL");
    productImageUrls = new HashSet<String>();
    productImageUrls.add("images/hiddenSeam/productTiles/pic3.jpg");
    product.setColors(colors);
    product.setCurrency(Currency.RUPEE);
    product.setDescription("Sample description");
    product.setPrice(220);
    product.setProductCategory(productCategory);
    product.setProductImageUrls(productImageUrls);
    product.setSizes(sizes);
    pm.insertDetails(product);


    product = new Product();
    colors = new HashSet<String>();
    colors.add("yellow");
    colors.add("black");
    colors.add("orange");
    colors.add("red");
    sizes = new HashSet<String>();
    sizes.add("XS");
    sizes.add("S");
    sizes.add("M");
    sizes.add("L");
    sizes.add("XL");
    sizes.add("XXL");
    productImageUrls = new HashSet<String>();
    productImageUrls.add("images/hiddenSeam/productTiles/pic7.jpg");
    product.setColors(colors);
    product.setCurrency(Currency.RUPEE);
    product.setDescription("Sample description");
    product.setPrice(119);
    product.setProductCategory(productCategory);
    product.setProductImageUrls(productImageUrls);
    product.setSizes(sizes);
    pm.insertDetails(product);

    product = new Product();
    colors = new HashSet<String>();
    colors.add("yellow");
    colors.add("white");
    colors.add("red");
    colors.add("brown");
    sizes = new HashSet<String>();
    sizes.add("XS");
    sizes.add("S");
    sizes.add("M");
    sizes.add("L");
    sizes.add("XL");
    sizes.add("XXL");
    productImageUrls = new HashSet<String>();
    productImageUrls.add("images/hiddenSeam/productTiles/pic2.jpg");
    product.setColors(colors);
    product.setCurrency(Currency.RUPEE);
    product.setDescription("Sample description");
    product.setPrice(200);
    product.setProductCategory(productCategory);
    product.setProductImageUrls(productImageUrls);
    product.setSizes(sizes);
    pm.insertDetails(product);
%>