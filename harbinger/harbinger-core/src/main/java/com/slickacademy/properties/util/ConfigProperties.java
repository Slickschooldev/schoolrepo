package com.slickacademy.properties.util;

import java.util.Properties;

/**
 * Created by ganesh on 8/6/15.
 */
public class ConfigProperties {
    private static ConfigProperties configProperties = null;
    private Properties properties;

    private ConfigProperties() {
        properties = new Properties();
    }

    public static ConfigProperties getInstance() {
        if (configProperties != null) {
            return configProperties;
        } else {
            configProperties = new ConfigProperties();
            return configProperties;
        }
    }

    public String getConfigValue(String key) {
        return properties.getProperty(key);
    }

    public Properties getProperties() {
        return properties;
    }

    /**
     * @param newProperties
     *            the properties to set
     */
    public void setProperties(Properties newProperties) {
        this.properties = newProperties;
    }
}
