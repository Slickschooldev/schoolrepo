package com.slickacademy.product;

import com.slickacademy.db.util.PersistenceManager;
import com.slickacademy.ecomm.model.Product;
import com.slickacademy.exception.HarbingerException;
import com.slickacademy.gui.ProductGui;
import com.slickacademy.model.*;
import com.slickacademy.model.Currency;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 * Created by hemanth on 4/6/16.
 */
public class ProductManager {
    private PersistenceManager pm;

    public ProductManager() {
        pm = PersistenceManager.getInstance();
    }

    public List<Product> getAllProducts() {
        List<Product> products = null;
        try {
            products = (List<Product>) pm.getAllDetails(Product.class.getSimpleName());
        } catch (HarbingerException e) {
            e.printStackTrace();
        }
        return products;
    }

    public List<Product> getProducts(String productCategoryId) {
        List<Product> products = null;
        try {
            products = (List<Product>) pm.searchDetailsBySearchAttributes(Product.class.getSimpleName(), "productCategory.id=" + productCategoryId);
        } catch (HarbingerException e) {
            e.printStackTrace();
        }
        return products;
    }

    public Product getProduct(long productId) {
        Connection connection = pm.getJDBCConnection();
        Product product = new Product();
        if (connection != null) {
            try {
                StringBuilder query = new StringBuilder();
                query.append("select DISTINCT a.color as color, b.currency, b.price, c.productImageUrl as productImageUrl, b.description, d.size from colors as a, product as b, productImageUrls as c, sizes as d ")
                        .append(" where b.id=").append(productId).append(" and a.id=b.id and c.id=b.id and d.id=b.id");
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query.toString());
                Set<String> colors = new HashSet<String>(), productImageUrls = new HashSet<String>(), sizes = new HashSet<String>();
                while (resultSet.next()) {
                    colors.add(resultSet.getString("color"));
                    productImageUrls.add(resultSet.getString("productImageUrl"));
                    sizes.add(resultSet.getString("size"));
                    product.setCurrency(Currency.values()[resultSet.getInt("currency")]);
                    product.setPrice(resultSet.getDouble("price"));
                    product.setDescription(resultSet.getString("description"));
                }
                product.setProductImageUrls(productImageUrls);
                product.setSizes(sizes);
                product.setColors(colors);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return product;
    }

    public static void main(String[] args) {
        ProductManager productManager = new ProductManager();

    }

}
