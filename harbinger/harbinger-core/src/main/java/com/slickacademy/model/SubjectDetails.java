package com.slickacademy.model;

import com.slickacademy.db.util.BaseEntity;

/**
 * Created by govind on 30/6/15.
 */
public class SubjectDetails extends BaseEntity {

    private ClassHierarchy classHierarchy;
    private Subject subject;
    private Faculty faculty;

    public ClassHierarchy getClassHierarchy() {
        return classHierarchy;
    }

    public void setClassHierarchy(ClassHierarchy classHierarchy) {
        this.classHierarchy = classHierarchy;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }
}
