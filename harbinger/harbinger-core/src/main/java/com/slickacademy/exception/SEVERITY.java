package com.slickacademy.exception;

/**
 * Use to set the Severity/ how effective the Error Code is.
 * 
 * @author rajuGT
 */
public enum SEVERITY {
    /**
     * list of Severity levels and its corresponding integer value.
     */
    WARN(0), MINOR(1), MAJOR(2), CRITICAL(3);

    /**
     * instance variable i
     */
    private final int i;

    /**
     * Constructor
     * 
     * @param i 
     */
    SEVERITY(int i) {
        this.i = i;
    }

}
