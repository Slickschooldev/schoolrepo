package com.slickacademy.exception;


public enum CommonError implements IError {
    UKNOWN_ERROR("CMN-0001", "An unknown error occured", SEVERITY.MAJOR, "Please try again after some time"),
    DB_SESSION_ERROR("CMN-0002", "Session closed unexpectedly", SEVERITY.MAJOR, "check connection"),
    QUERY_ERROR("CMN-0003", "Query is wrong", SEVERITY.MAJOR, "check query"),
    CLASS_NOT_FOUND_ERROR("CMN-0004", "Failed to load the requested class", SEVERITY.CRITICAL, "check given class name is correct"),
    ILLEGAL_ACCESS_ERROR("CMN-0005", "Does not have permissions", SEVERITY.MAJOR, "check file permissions"),
    NULL_POINTER_ERROR("CMN-0006", "unable to open session", SEVERITY.CRITICAL, "try to open the session again later"),
    FAILED_TO_EXECUTE_NAMEDQUERY("CMN-0007", "failed to execute named query", SEVERITY.MAJOR,"Please check the log files"),
    FAILED_TO_SAVE_RECORD("CMN-0008","failed to save record",SEVERITY.MAJOR,"Please check the log files"),
    FAILED_TO_DELETE_RECORD("CMN-0009","failed to delete record",SEVERITY.MAJOR,"Please check the log files"),
    QUARTZ_SCHEDULER_FAILED("CMN-0010","Scheduler failed",SEVERITY.MAJOR,"Please check the log files"),
    FAILED_TO_SEND_MAIL("CMN-0011","Mail sending failed",SEVERITY.MAJOR,"Please check the log files"),
    FAILED_TO_WRITE_AUDITLOG("CMN-0012","Audit log writing failed",SEVERITY.MAJOR,"Please check the log files"),
    FAILED_TO_CREATE_AUDIT_LOG_OBJECT("CMN-0013","Audit log object creation failed",SEVERITY.MAJOR,"Please check the log files"),
    FAILED_TO_INITIALIZE("CMN-0014","Failed to intialize", SEVERITY.MAJOR, "Please check the log files"),
    FAILED_TO_READ_CONFIG_PARAMETER("CMN-0015","Failed to get a parameter from config table", SEVERITY.MINOR, "Please check the log files"),
    NUMBER_FORMAT_ERROR("CMN-0016","Number format error", SEVERITY.MINOR, "Please check the log files"),
    TRANSACTION_ERROR("CMN-0017","Transaction error", SEVERITY.MAJOR, "Please check the log files"),
    FAILED_TO_FETCH_FROM_CACHE("CMN-0018","Object not found in cache", SEVERITY.MINOR, "Check the database entries"),
    FAILED_TO_FETCH_FROM_DATABASE("CMN-0019","Failed to fetch from database", SEVERITY.MAJOR, "Check the database connectivity or the database entries"),
    FAILED_TO_EXECUTE_SQL_STATEMENT("CMN-0020","Failed to execute SQL statement", SEVERITY.MAJOR, "Please check the log files"),
    MEMORY_ERROR("CMN-0021","Memory error", SEVERITY.MAJOR, "Please check the log files"),
    CLASS_NOT_FOUND("CMN-0022", "Failed to load the requested class", SEVERITY.CRITICAL, "check given class name is correct"),
    IO_EXCEPTION_ERROR("CMN-0023","FtpClient methods failed",SEVERITY.MAJOR,"Please check the logs"),
    FILE_NOT_FOUND_EXCEPTION("CMN-0024","File not found exception",SEVERITY.MAJOR,"Check if the file exists"),
    JSCH_EXCEPTION("CMN-0025","Failed to obtain session for sftp due to incorrect username or password.",SEVERITY.MAJOR,"Please check SFTP credentials"),
    SFTP_EXCEPTION("CMN-0026","Sftp failed",SEVERITY.MAJOR,"Check the credentials for sftp"),
    ILLEGAL_OBJECT_ACCESS_ERROR("CMN-0027", "Illegal access", SEVERITY.MAJOR, "Check the logs"),
    FTP_CONNECT_ERROR("CMN-0028","Failed to connect to ftp",SEVERITY.MAJOR,"Check the credentials for ftp access"),
    DIRECTORY_PATH_NOT_FOUND("CMN-0029","Failed to find the directory",SEVERITY.MAJOR,"Check if the directory path exists"),
    CONCURRENT_MODIFICATION_ERROR("CMN-0030"," Operation failed, because someone has modified the object concurrently",SEVERITY.MINOR,"Try after some time"),
    CACHE_CREATION_FAILED_ILLEGAL_STATUS("CMN-0031","Cache Creation failed because of illegal status",SEVERITY.CRITICAL,"Please check the cache status"),
    CACHE_KEY_NOT_REMOVED_ILLEGAL_STATUS("CMN-0032", "Failed to remove key from cache", SEVERITY.MAJOR, "Please check the cache status and statitics."),
    CONSTRAINT_VIOLATION_ERROR("CMN-0033","DB constraint has been violated",SEVERITY.MINOR,"Please check the log files"),
    DATA_EXCEPTION("CMN-0034","Some illegal operation, mismatched types or incorrect cardinality",SEVERITY.MINOR,"Please check the log files"),
    JDBC_CONNECTION_EXCEPTION("CMN-0035","Not able to connect to the database",SEVERITY.MINOR,"Please check the database setup"),
    INTERRUPTED_EXCEPTION("CMN-0036","Failed to exceute Thread sleep",SEVERITY.MINOR,"Please wait"),
    FAILED_TO_UPLOAD_FILE("CMN-0037","Failed to upload files",SEVERITY.MAJOR,"Please check if the directory path exists for upload and the absolute file path"),
    FAILED_TO_LIST_FILES("CMN-0038","Failed to list files",SEVERITY.MAJOR,"Check the path and sftp connectivity"),
    ZARADA_DATE_FORMAT_ERROR("CMN-0039","Failed to parse the given date from locale format",SEVERITY.MAJOR,"Please check the format of date string passed"),
    ENTRY_NOT_EXISTING_IN_DB("CMN-0040","The entry being fetched has been deleted in a concurrent operation",SEVERITY.MAJOR,"Please create it again after verifying it no longer exists");

    
    private final String errorCode;
    private final String description;
    private final SEVERITY severity;
    private final String hint;

    private CommonError(String errorCode, String description, SEVERITY severity, String hint) { 
        this.errorCode = errorCode;
        this.description = description;
        this.severity = severity;
        this.hint = hint;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getDescription() {
        return description;
    }

    public SEVERITY getSeverity() {
        return severity;
    }

    public String getHint() {
        return hint;
    }
}
