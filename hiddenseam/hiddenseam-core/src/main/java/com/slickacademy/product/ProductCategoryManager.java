package com.slickacademy.product;

import com.slickacademy.db.util.PersistenceManager;
import com.slickacademy.ecomm.model.ProductCategory;
import com.slickacademy.exception.HarbingerException;

import java.util.List;

/**
 * Created by hemanth on 4/6/16.
 */
public class ProductCategoryManager {
    private PersistenceManager pm;

    public ProductCategoryManager() {
        pm = PersistenceManager.getInstance();
    }

    public List<ProductCategory> getAllProductCategories() {
        List<ProductCategory> productCategories = null;
        try {
            productCategories = (List<ProductCategory>)pm.getAllDetails(ProductCategory.class.getSimpleName());
        } catch (HarbingerException e) {
            e.printStackTrace();
        }
        return productCategories;
    }


}
