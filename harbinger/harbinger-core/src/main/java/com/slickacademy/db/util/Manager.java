/**
 * 
 */
package com.slickacademy.db.util;

import java.net.UnknownHostException;

import javax.servlet.ServletContext;

import com.slickacademy.exception.HarbingerException;
import com.slickacademy.xml.XMLFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.context.ServletContextAware;


/**
 * @author dev
 * 
 */
public abstract class Manager implements ApplicationContextAware,
        ServletContextAware {

    private XMLFactory xmlFactory         = null;
    private ApplicationContext applicationContext = null;
    private ServletContext servletContext ;
    private String             entityName         = null;

    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * @param entityName
     *            the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /**
     * @return the xml
     */
    public XMLFactory getXmlFactory() {
        return xmlFactory;
    }

    /**
     * @param xmlFactory
     *            the xml to set
     */
    public void setXmlFactory(XMLFactory xmlFactory) {
        this.xmlFactory = xmlFactory;
    }

    public abstract void init() throws HarbingerException,
            UnknownHostException;

    /**
     * @return the applicationContext
     */
    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * @param applicationContext
     *            the applicationContext to set
     */
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void setServletContext(ServletContext servletContext) {
        this.servletContext=servletContext;

    }

    public ServletContext getServletContext() {
        return servletContext;
    }
}
