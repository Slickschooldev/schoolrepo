<%@ page import="com.slickacademy.db.util.PersistenceManager" %>
<%@ page import="com.slickacademy.ecomm.model.ProductCategory" %>
<%--
  Created by IntelliJ IDEA.
  User: hemanth
  Date: 4/6/16
  Time: 12:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%
  PersistenceManager pm = PersistenceManager.getInstance();
  ProductCategory productCategory = new ProductCategory();
  productCategory.setBannerImageUrl("images/hiddenSeam/mainBanner/slide1.jpg");
  productCategory.setName("Shirts");
  pm.insertDetails(productCategory);
  productCategory = new ProductCategory();
  productCategory.setBannerImageUrl("images/hiddenSeam/mainBanner/slide2.jpg");
  productCategory.setName("Tops");
  pm.insertDetails(productCategory);
  productCategory = new ProductCategory();
  productCategory.setBannerImageUrl("images/hiddenSeam/mainBanner/slide3.jpg");
  productCategory.setName("Sweaters");
  pm.insertDetails(productCategory);
%>