<div class="landing">
    <div class="container">
        <header>
            <div class="clearfix">
                <ul class="nav_left">
                    <li><a href="https://www.facebook.com/hiddenseam" target="_blank"><img
                            src="../../images/facebook.png"></a></li>
                    <li><a href="https://www.instagram.com/hiddenseam/" target="_blank"><img
                            src="../../images/instagram.png"></a></li>
                </ul>

                <ul class="top-nav">
                    <li class="cart"><a href="cart">Cart</a></li>
                    <li class="user"><a href="my-account">User</a></li>
                </ul>
            </div>
            <h1><a href="#"><img src="../../images/logo_landing.png" alt="[Image: Hidden Seam]"></a></h1>
        </header>
        <div class="main">
            <h2><span class="left-flower"></span>Summer 2016<span class="right-flower"></span></h2>

            <p class="view-collection">
                <a href="../../index.jsp">View Collection</a>
            </p>
        </div>
        <footer>
            <div class="clearfix"></div>
        </footer>
    </div>
</div>

<link href="../../css/reset.css" rel="stylesheet">
<link href="../../css/bootstrap-responsive.css" rel="stylesheet">
<link href="../../css/landing.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Josefin+Sans:300" rel="stylesheet" type="text/css">