/**
 * 
 */
package com.slickacademy.xml;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author bgh08159
 * 
 */
public class BasicConfiguration {
	private Properties properties;
	private static Logger logger = Logger.getLogger(BasicConfiguration.class);

	public BasicConfiguration(String propertiesFileName) {
		if (properties == null) {
			properties = new Properties();
			try {
				properties.load(new FileInputStream(propertiesFileName));
			} catch (IOException ex) {
				logger.error(ex);
			}
		}
	}

	public BasicConfiguration() {
		this("default");
	}

	public String getValue(String key) {
		return properties.getProperty(key);
	}

}
