package com.slickacademy.model;

/**
 * Created by govind on 30/6/15.
 */
public enum Status {
    ACTIVE(0),
    INACTIVE(1);

    private final int status;

    private Status(int status) {
        this.status = status;
    }
}
