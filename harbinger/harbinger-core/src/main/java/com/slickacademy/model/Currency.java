package com.slickacademy.model;

/**
 * Created by govind on 17/5/16.
 */
public enum Currency {
    EURO(0, "€"),
    DOLLAR(1, "$"),
    RUPEE(2, "Rs");

    private int i;
    private String symbol;

    Currency(int i, String symbol) {
        this.i = i;
        this.symbol = symbol;
    }

    public String getSymbol() {
        return this.symbol;
    }
}
