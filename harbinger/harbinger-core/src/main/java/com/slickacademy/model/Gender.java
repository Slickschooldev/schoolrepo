package com.slickacademy.model;

/**
 * Created by govind on 17/5/16.
 */
public enum Gender {
    MALE(0),
    FEMALE(1),
    OTHER(2);

    private int gender;

    Gender(int i) {
        this.gender = i;
    }
}
