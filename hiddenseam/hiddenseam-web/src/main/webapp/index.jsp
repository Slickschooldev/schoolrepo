<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ page import="com.slickacademy.product.ProductCategoryManager" %>
<%@ page import="java.util.List" %>
<%@ page import="com.slickacademy.ecomm.model.ProductCategory" %>

<!DOCTYPE HTML>
<html>
<head>
	<title>Hiddenseam</title>
	<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<script src="js/jquery.min.js"></script>
	<!--<script src="js/jquery.easydropdown.js"></script>-->
	<!--start slider -->
	<link rel="stylesheet" href="css/fwslider.css" media="all">
	<link rel="stylesheet" href="css/indexPage.css" media="all">
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/fwslider.js"></script>
	<!--end slider -->
	<script type="text/javascript">
		$(document).ready(function() {
			$(".dropdown img.flag").addClass("flagvisibility");

			$(".dropdown dt a").click(function() {
				$(".dropdown dd ul").toggle();
			});

			$(".dropdown dd ul li a").click(function() {
				var text = $(this).html();
				$(".dropdown dt a span").html(text);
				$(".dropdown dd ul").hide();
				$("#result").html("Selected value is: " + getSelectedValue("sample"));
			});

			function getSelectedValue(id) {
				return $("#" + id).find("dt a span.value").html();
			}

			$(document).bind('click', function(e) {
				var $clicked = $(e.target);
				if (! $clicked.parents().hasClass("dropdown"))
					$(".dropdown dd ul").hide();
			});


			$("#flagSwitcher").click(function() {
				$(".dropdown img.flag").toggleClass("flagvisibility");
			});
		});
	</script>
</head>
<body>
<div class="header">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="header-left">
					<div class="logo">
						<a href="index.jsp"><img src="images/logo.png" alt="HiddenSeam"/></a>
					</div>
					<div class="menu">
						<a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
						<ul class="nav" id="nav">
                            <li><a>E-Shop</a>
                                <ul>
                                    <li><a href="shop.jsp?id=1">Tops</a></li>
                                    <li><a href="shop.jsp?id=2">Shirts</a></li>
                                    <li><a href="shop.jsp?id=3">Sweaters</a></li>
                                </ul>
                            </li>
                            <li><a>Exchange Policy</a></li>
                            <li><a>Privacy Policy</a></li>
                            <li><a>About</a></li>
                            <li><a href="contact.html">Contact Us</a></li>
                            <div class="clear"></div>
						</ul>
						<script type="text/javascript" src="js/responsive-nav.js"></script>
					</div>
					<div class="clear"></div>
				</div>
				<div class="header_right">
					<!-- start search-->
					<div class="search-box">
						<div id="sb-search" class="sb-search">
							<form>
								<input class="sb-search-input" placeholder="Enter your search term..." type="search" name="search" id="search">
								<input class="sb-search-submit" type="submit" value="">
								<span class="sb-icon-search"> </span>
							</form>
						</div>
					</div>
					<!----search-scripts---->
					<script src="js/classie.js"></script>
					<script src="js/uisearch.js"></script>
					<script>
						new UISearch( document.getElementById( 'sb-search' ) );
					</script>
					<!----//search-scripts---->
					<ul class="icon1 sub-icon1 profile_img">
						<li><a class="active-icon c1" href="#"> </a>
							<ul class="sub-icon1 list">
								<div class="product_control_buttons">
									<a href="#"><img src="images/edit.png" alt=""/></a>
									<a href="#"><img src="images/close_edit.png" alt=""/></a>
								</div>
								<div class="clear"></div>
								<li class="list_img"><img src="images/1.jpg" alt=""/></li>
								<li class="list_desc"><h4><a href="#">velit esse molestie</a></h4><span class="actual">1 x
                          $12.00</span></li>
								<div class="login_buttons">
									<div class="check_button"><a href="checkout.html">Check out</a></div>
									<div class="login_button"><a href="login.html">Login</a></div>
									<div class="clear"></div>
								</div>
								<div class="clear"></div>
							</ul>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="banner">
	<!-- start slider -->
	<div id="fwslider">
		<div class="slider_container">
			<%
				ProductCategoryManager productCategoryManager = new ProductCategoryManager();
				List<ProductCategory> productCategories = productCategoryManager.getAllProductCategories();
				if(productCategories != null && productCategories.size()>0) {
					for(ProductCategory productCategory:productCategories) {
			%>
			<div class="slide">
				<img src="<%=productCategory.getBannerImageUrl()%>" class="img-responsive" alt="image not found"/>
				<!-- Texts container -->
				<div class="slide_content">
					<div class="slide_content_wrap">
						<!-- Text title -->
						<h1 class="title">New Collections<br>Available</h1>
						<!-- /Text title -->
						<div class="button">
							<a class="bannerDetails" style="background: none;" href="shop.jsp?id=<%=productCategory.getId()%>">
								See Details
							</a></div>
					</div>
				</div>
				<!-- /Texts container -->
			</div>
			<%
					}
				}
			%>
			<!--/slide -->
		</div>
		<div class="timers"></div>
		<div class="slidePrev"><span></span></div>
		<div class="slideNext"><span></span></div>
	</div>
	<!--/slider -->
</div>

<div class="features">
	<div class="container">
		<h3 class="m_3">Featured products</h3>
		<!--<div class="close_but"><i class="close1"> </i></div>-->
		<div class="row">
			<div class="col-md-3 top_box">
				<div class="view view-ninth"><a href="single.html">
					<img src="images/hiddenSeam/featuredProducts/fp1.jpg" class="img-responsive" alt=""/>
					<div class="mask mask-1"> </div>
					<div class="mask mask-2"> </div>
					<div class="content">
						<h2>Hover Style #9</h2>
						<p>One line description about product.</p>
					</div>
				</a> </div
			</div>
			<h4 class="m_4"><a href="#">Product name-1</a></h4>
			<p class="m_5">Price or short desc.</p>
		</div>
		<div class="col-md-3 top_box">
			<div class="view view-ninth"><a href="single.html">
				<img src="images/hiddenSeam/featuredProducts/fp2.jpg" class="img-responsive" alt=""/>
				<div class="mask mask-1"> </div>
				<div class="mask mask-2"> </div>
				<div class="content">
					<h2>Hover Style #9</h2>
					<p>One line description about product.</p>
				</div>
			</a> </div>
			<h4 class="m_4"><a href="#">Product name-2</a></h4>
			<p class="m_5">Price or short desc</p>
		</div>
		<div class="col-md-3 top_box">
			<div class="view view-ninth"><a href="single.html">
				<img src="images/hiddenSeam/featuredProducts/fp3.jpg" class="img-responsive" alt=""/>
				<div class="mask mask-1"> </div>
				<div class="mask mask-2"> </div>
				<div class="content">
					<h2>Hover Style #9</h2>
					<p>One line description about product.</p>
				</div>
			</a> </div>
			<h4 class="m_4"><a href="#">Product name-3</a></h4>
			<p class="m_5">Price or short desc</p>
		</div>
		<div class="col-md-3 top_box1">
			<div class="view view-ninth"><a href="single.html">
				<img src="images/hiddenSeam/featuredProducts/fp4.jpg" class="img-responsive" alt=""/>
				<div class="mask mask-1"> </div>
				<div class="mask mask-2"> </div>
				<div class="content">
					<h2>Hover Style #9</h2>
					<p>One line description about product.</p>
				</div>
			</a> </div>
			<h4 class="m_4"><a href="#">nostrud exerci ullamcorper</a></h4>
			<p class="m_5">claritatem insitam</p>
		</div>
	</div>
</div>
</div>
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <ul class="footer_box">
                    <h4>Products</h4>
                    <li><a href="#">Men</a></li>
                    <li><a href="#">Women</a></li>
                    <li><a href="#">Youth</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="footer_box">
                    <h4>Customer Support</h4>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Shipping and Order Tracking</a></li>
                    <li><a href="#">Easy Returns</a></li>
                    <li><a href="#">Warranty</a></li>
                    <li><a href="#">Replacement Binding Parts</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="footer_box">
                    <h4>Newsletter</h4>
                    <div class="footer_search">
                        <form>
                            <input type="text" value="Enter your email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter your email';}">
                            <input type="submit" value="Go">
                        </form>
                    </div>
                    <ul class="social">
                        <li class="facebook"><a href="#"><span> </span></a></li>
                        <li class="twitter"><a href="#"><span> </span></a></li>
                        <li class="instagram"><a href="#"><span> </span></a></li>
                        <li class="pinterest"><a href="#"><span> </span></a></li>
                        <li class="youtube"><a href="#"><span> </span></a></li>
                    </ul>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>